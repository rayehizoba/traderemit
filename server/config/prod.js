"use strict";

let path = require("path");
let pkg = require("../../package.json");

module.exports = {
	app: {
		googleAnalyticsID: "UA-105624523-1",
		url: "www.traderemit.com"
	},

	port: process.env.PORT || 8000,

	db: {
		uri: process.env.MONGODB_URI || "mongodb://localhost/" + pkg.config.dbName,
		options: {
			user: process.env.MONGO_USERNAME || "",
			pass: process.env.MONGO_PASSWORD || ""
		}
	},

	redis: {
		enabled: true,
		uri: process.env.REDISCLOUD_URL || "redis://localhost:6379",
		options: null
	},

	sessions: {
		cookie: {
			// secure cookie should be turned to true to provide additional
			// layer of security so that the cookie is set only when working
			// in HTTPS mode.
			// secure: true
		}
	},

	// Mail sending settings
	mailer: {
		transport: "mailgun",
		mailgun: {
			apiKey: "key-da7fbbcd296887021da3bf049ce0927e",
			domain: "traderemit.com"
		}
	},

	test: false,

	features: {
		disableSignUp: false,
	},

	logging: {
		console: {
			level: "debug"
		},

		file: {
			enabled: true,
			path: path.join(global.rootPath, "logs"),
			level: "info",
			json: false,
			exceptionFile: true
		},

		graylog: {
			enabled: false
			// servers: [ { host: "192.168.0.174", port: 12201 } ]
		},

		papertrail: {
			enabled: false,
			host: null,
			port: null,
			level: "debug",
			program: "vem"
		},

		logentries: {
			enabled: false,
			token: null
		},

		loggly: {
			enabled: false,
			token: null,
			subdomain: null
		},

		logsene: {
			enabled: false,
			token: null
		},

		logzio: {
			enabled: false,
			token: null
		}

	},

	agendaTimer: "one minute",

	blocktrail: {
		// isTestnet: false,
		webhookUrl: "https://www.traderemit.com/webhook/user/",
	},

	admin: {
		username: "administrator"
	},
};
