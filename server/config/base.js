"use strict";

let path = require("path");
let pkg = require("../../package.json");

module.exports = {
	app: {
		title: pkg.title,
		version: pkg.version,
		description: pkg.description,
		keywords: pkg.keywords.join(","),
		url: "http://localhost:" + (process.env.PORT || 8080) + "/",
		//googleAnalyticsID: 'UA-xxxxx-x',
		contactEmail: "support@traderemit.com"
	},

	ip: process.env.NODE_IP || "0.0.0.0",
	port: process.env.PORT || 8080,

	rootPath: global.rootPath,
	dataFolder: path.join(global.rootPath, "data"),

	uploadLimit: 2 * 1024 * 1024, // 2MB

	sessions: {
		cookie: {
			// session expiration is set by default to one week
			maxAge: 7 * 24 * (60 * 60 * 1000),

			// httpOnly flag makes sure the cookie is only accessed
			// through the HTTP protocol and not JS/browser
			httpOnly: true,

			// secure cookie should be turned to true to provide additional
			// layer of security so that the cookie is set only when working
			// in HTTPS mode.
			secure: false
		},

		// Cookie key name
		name: "sessionId",

		// Mongo store collection name
		collection: "sessions"
	},

	// Mail sending settings
	mailer: {
		enabled: true,
		from: "TradeRemit <noreply@traderemit.com>",
		transport: "mailgun",
		mailgun: {
			apiKey: "key-da7fbbcd296887021da3bf049ce0927e",
			domain: "sandbox8b0f749b2ba94898a842338eda935a58.mailgun.org"
		}
	},

	test: false,

	db: {
		uri: process.env.MONGO_URI || "mongodb://localhost/" + pkg.config.dbName + "-dev",
		options: {
			user: "",
			pass: "",
			server: {
				socketOptions: {
					keepAlive: 1
				}
			}
		}
	},

	redis: {
		enabled: false,
		uri: process.env.REDIS_URI || "redis://localhost:6379",
		options: null
	},

	cacheTimeout: 30, // 30 seconds
	// cacheTimeout: 2 * 60, // 2 mins

	features: {
		disableSignUp: false,
		verificationRequired: true
	},

	authKeys: {

		google: {
			clientID: null,
			clientSecret: null
		},

		facebook: {
			clientID: null,
			clientSecret: null
		},

		github: {
			clientID: null,
			clientSecret: null
		},

		twitter: {
			clientID: null,
			clientSecret: null
		}
	},

	logging: {
		console: {
			level: "debug"
		},

		file: {
			enabled: false,
			path: path.join(global.rootPath, "logs"),
			level: "info",
			json: false,
			exceptionFile: true
		},

		graylog: {
			enabled: false
			// servers: [ { host: "192.168.0.174", port: 12201 } ]
		},

		papertrail: {
			enabled: false,
			host: null,
			port: null,
			level: "debug",
			program: "vem"
		},

		logentries: {
			enabled: false,
			token: null
		},

		loggly: {
			enabled: false,
			token: null,
			subdomain: null
		},

		logsene: {
			enabled: false,
			token: null
		},

		logzio: {
			enabled: false,
			token: null
		}

	},

	agendaTimer: "one minute",

	blocktrail: {
		isTestnet: false,
		webhookUrl: "https://traderemit.localtunnel.me/webhook/user/",
	},

	admin: {
		username: "admin",
	}
};
