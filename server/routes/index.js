"use strict";

let config 	= require("../config");
let logger 	= require("../core/logger");
let path 	= require("path");

module.exports = function(app, db) {

	// Force redirect to https://...
	app.get("*", (req, res, next) => {
		if (config.isProductionMode()) {
			let expr = /herokuapp.com/;
			let isHerokuapp = expr.test(req.host);
			let isNotHttps = req.headers["x-forwarded-proto"] != "https";
			if(isHerokuapp || isNotHttps)
				res.redirect(`https://www.traderemit.com${ req.url }`);
			else
				next();
		} else next();
	});

	// Handle health check routes
	require("./health")(app, db);

	// Handle account routes
	require("./account")(app, db);

	// Handle Auth routes
	require("./auth")(app, db);

	// Load services routes
	//require("../applogic/routeHandlers")(app, db);
	let services = require("../core/services");
	services.registerRoutes(app, db);

	// Handle Graphql request
	require("./graphql")(app, db);

	// Handle webhook
	require("./webhook")(app, db);	

	// Handle identity verification
	require("./verification")(app, db);	

	// Index page
	app.get("/*", function(req, res) {
		if (req.user != null)
			res.render("main", {
				user: req.user
			});
		else
			// res.render("index");
			res.render("main");
	});

	// Handle errors
	require("./errors")(app, db);	
};
