"use strict";

let config = require("../config");
let logger = require("../core/logger");
let C = require("../core/constants");

let async = require("async");
let express = require("express");
let _ = require("lodash");
let multer = require("multer");
let fs = require("fs");
let cloudinary = require('cloudinary');
let mailer = require("../libs/mailer");

cloudinary.config({
	cloud_name: config.cloudinary.cloudName,
	api_key: config.cloudinary.apiKey,
	api_secret: config.cloudinary.apiSecret
});

let User = require("../models/user");
let IdVerificationUpload = require("../models/IdVerificationUpload");

module.exports = function (app, db) {
	app.post(
		"/identity-verification",
		multer({ dest: "./uploads/" }).any(),
		(req, res) => {
			let file = {
				"mimetype": req.files[0].mimetype,
				"b64": new Buffer(fs.readFileSync(req.files[0].path)).toString("base64") // change this is asynchronous read
			};
			fs.unlink(req.files[0].path);

			cloudinary.v2.uploader.upload(`data:${file.mimetype};base64,${file.b64}`,
				(error, result) => {
					if (error) {
						logger.error(error);
						return res.sendStatus(500);
					}

					let upload = new IdVerificationUpload({
						username: req.user.username,
						imageUrl: result.secure_url
					});

					return upload.save()
						.then(doc => {
							// if this upload forms a pair
							// update user verification status to pending
							// and send email to admin
							let filter = { username: req.user.username };
							return IdVerificationUpload.count(filter, (err, count) => {
								if (count % 2 === 0) {
									// update user verification status to pending
									let update = {
										idVerificationStatus: C.ID_VERIFICATION_STATUS_PENDING
									};
									return User.findByIdAndUpdate(req.user.id, update).exec()
										.then(user => {
											// send email to admin
											if (!config.mailer.enabled) {
												const err = "Trying to send email without config.mailer not enabled; emailing skipped. Have you configured mailer yet?";
												logger.error(err);
												return res.sendStatus(200);
											}
											let subject = `Identity verification upload from ${user.username}`;

											res.render("mail/newIdVerificationUpload", {
												name: user.username
											}, function (err, html) {
												if (err) return logger.error(err);

												mailer.send(config.admin.email, subject, html, function (err, info) {
													if (err) logger.error("Unable to send Identity verification request email to admin");
												});
											});
											return res.sendStatus(200);
										});
								} else {
									return res.sendStatus(200);
								}
							});
						});
				});
		});
};
