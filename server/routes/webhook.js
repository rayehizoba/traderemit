"use strict";

let config = require("../config");
let logger = require("../core/logger");

let C = require("../core/constants");
let tokgen = require("../libs/tokgen");

let crypto = require("crypto");
let async = require("async");
let passport = require("passport");
let express = require("express");
let _ 				= require("lodash");
let blocktrailClient = require("../core/blocktrailClient");

let response = require("../core/response");
let Sockets = require("../core/sockets");

let User = require("../models/user");

module.exports = function (app, db) {
	// wallet webhook
	app.post("/webhook/user/:id", (req, res) => {
		if (!req.query.token && (req.query.token !== config.blocktrail.webhookToken)) {
			return res.status(401).send({ error: "You are not authorized to access this resource!" });
		}
		if (req.body.data.confirmations > 4) return;
		if (req.body.event_type !== "address-transactions") return;

		// find this user
		User.findById(req.params.id, (err, user) => {
			if (err) return logger.error(err);

			let client = blocktrailClient.new();
			client.initWallet(
				user.walletName,
				user.walletPassword,
				(err, wallet) => {
					if (err) return logger.error(err);

					// get user wallet balance
					wallet.getBalance((err, confirmedBalance, unconfirmedBalance) => {
						if (err) return logger.error(err);
						user.mainBalance = confirmedBalance;
						user.unconfirmedBalance = unconfirmedBalance;
						user.save();
					});
				}
			);

		});

	});
};
