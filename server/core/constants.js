"use strict";

let C = {};

C.append = function(items, prefix) {
	items.forEach((item) => {
		let name = item.toUpperCase();
		if (prefix)
			name = prefix + "_" + name;
		C[name] = item;
	});
};

/**
 * User role constants
 */
C.append([
	"admin",
	"user",
	"guest"
], "ROLE");

/**
 * User permission constants
 */
C.append([
	"admin",
	"owner",
	"loggedIn",
	"public"
], "PERM");

/**
 * Response error reasons
 */
C.append([
	"VALIDATION_ERROR",
	"INVALID_CODE",
	"MODEL_NOT_FOUND",
	"ONLY_OWNER_CAN_EDIT_AND_DELETE"
], "ERR");

/**
 * Trade summary types
 */
C.append([
	"RELEASED",
	"CANCELLED",
	"EXPIRED"
], "TRADE_SUMMARY");

/**
 * Trade notification types
 */
C.append([
	"NEW_TRADE",
	"NEW_MESSAGE",
	"REVIEW_GOOD",
	"REVIEW_BAD",
	"REVIEW_NEUTRAL",
	"RELEASED",
	"UNDER_DISPUTE",
	"EXPIRED",
	"CANCELLED",
	"PAID"
], "TRADE_NOTIF");

/**
 * Id verification status types
 */
C.append([
	"NOT_VERIFIED",
	"VERIFIED",
	"PENDING",
	"DECLINED"
], "ID_VERIFICATION_STATUS");

module.exports = C;

//console.log(C);
