"use strict";

let logger = require("./logger");
let config = require("../config");
let async = require("async");
let _ = require("lodash");

let moment = require("moment");
let chalk = require("chalk");
let Agenda = require("agenda");
let mailer = require("../libs/mailer");

let User = require("../models/user");
let Advertisement = require("../applogic/modules/advertisements/models/advertisement");

let agenda = new Agenda({
	db: {
		address: config.db.uri,
		collection: "agendaJobs"
	},
	processEvery: config.agendaTimer || "one minute"
});

agenda.on("fail", function (err, job) {
	return logger.error("Job failed with error: " + err);
});

/**
 * Remove unverified account after 24 hours
 */
agenda.define("removeUnverifiedAccounts", function (job, done) {
	logger.debug("Running 'removeUnverifiedAccounts' process...");
	try {

		User.remove({
			createdAt: {
				$lte: moment().subtract(1, "day").toDate()
			},
			verified: false
		}, (err, count) => {
			if (count > 0)
				logger.warn(chalk.bold.red(count + " unverified and expired account removed!"));

			done();
		});
	} catch (error) {
		logger.error("Job running exception!");
		logger.error(error);
		return done(error);
	}
});

/**
 * Send daily price newsletter every 24 hours
 */
agenda.define("sendDailyPrice", function (job, done) {
	logger.debug("Running 'sendDailyPrice' process...");
	// try {
	// 	let asyncTasks = [];

	// 	// get today's BTC price and best 5 sell ads
	// 	asyncTasks.push(callback => {
	// 		logger.debug("getting today's BTC price and best 5 sell ads...");
	// 		let filter = {};
	// 		filter.adType = "sell";
	// 		filter.visible = true;
	// 		filter.enabled = true;

	// 		Advertisement
	// 			.find(filter)
	// 			.sort("price")
	// 			.limit(5)
	// 			.exec()
	// 			.then(ads => {
	// 				callback(null, ads);
	// 			});
	// 	});

	// 	// get verified (email) and subscribed users
	// 	asyncTasks.push((ads, callback) => {
	// 		logger.debug("getting verified and subscribed users...");
	// 		let conditions = {
	// 			dailyPrice: true,
	// 			verified: true
	// 		};

	// 		User.find(conditions, (err, users) => {
	// 			if (err) return callback(err);
	// 			callback(null, ads, users);
	// 		});
	// 	});

	// 	// loop through each user and send daily price email
	// 	asyncTasks.push((ads, users, callback) => {
	// 		logger.debug("looping through each user and send daily price email...", ads, users);
	// 		if (!config.mailer.enabled)
	// 			callback("config.mailer not enabled; emailing skipped. Have you configured mailer yet?");

	// 		let bestPrice = ads[0].price;

	// 		_.each(users, user => {
	// 			let subject = `Today's Bitcoin price is: NGN${bestPrice}`;

	// 			logger.debug(app._app);
	// 			app.render("mail/bestPrice", {
	// 				name: user.username,
	// 				btcPrice: bestPrice,
	// 				ads: ads
	// 			}, function (err, html) {
	// 				if (err) return done(err);

	// 				mailer.send(user.email, subject, html, function (err, info) {
	// 					if (err)
	// 						logger.error(`Unable to send daily price email to ${user.email}`);

	// 					done(null, user);
	// 				});
	// 			});
	// 		});

	// 		callback();
	// 	});

	// 	async.waterfall(asyncTasks, error => {
	// 		logger.debug("hello from async waterfall callback!");
	// 		if (error) {
	// 			logger.error("Job running exception!");
	// 			logger.error(error);
	// 			return done(error);
	// 		}
	// 		done();
	// 	});
	// } catch (error) {
	// 	logger.debug("hello from catch block!");
	// 	logger.error("Job running exception!");
	// 	logger.error(error);
	// 	return done(error);
	// }
});

/**
 * Starting agenda
 */
agenda.on("ready", function () {
	if (config.isTestMode())
		return;

	agenda.every("8 hours", "removeUnverifiedAccounts");
	// agenda.every("3 minutes", "sendDailyPrice");
	agenda.start();
	logger.info(chalk.yellow("Agenda started!"));
});

module.exports = agenda;
