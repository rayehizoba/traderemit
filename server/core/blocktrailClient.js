let blocktrail = require("blocktrail-sdk");
let config = require("../config");

let self = {

	/**
		* Returns a new Blocktrail client object
		* which can be used to make Blocktrail api calls
		*
	 */
	new() {
		return blocktrail.BlocktrailSDK({
			apiKey: config.blocktrail.apiKey,
			apiSecret: config.blocktrail.apiSecret,
			network: "BTC",
			testnet: config.blocktrail.isTestnet
		});
	}

};

module.exports = self;