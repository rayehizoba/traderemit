"use strict";

let logger = require("./logger");
let config = require("../config");
let C = require("./constants");

// let _ 				= require("lodash");
// let tokgen = require("../libs/tokgen");
// let fakerator		= require("fakerator")();
let async = require("async");
let blocktrail = require("blocktrail-sdk");

let User = require("../models/user");

function randomStr() {
	let text = "";
	let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

	for (let i = 0; i < 10; i++)
		text += possible.charAt(Math.floor(Math.random() * possible.length));

	return text;
}

module.exports = function () {
	/**
	 * Create default `admin` and `test` users
	 */
	return User.find({}).exec().then(docs => {
		if (docs.length === 0) {
			logger.warn("Load default Users to DB...");

			let client = blocktrail.BlocktrailSDK({
				apiKey: config.blocktrail.apiKey,
				apiSecret: config.blocktrail.apiSecret,
				network: "BTC",
				testnet: config.blocktrail.isTestnet
			});
			let asyncTasks = [];

			// create admin wallet
			asyncTasks.push(callback => {
				logger.warn("Creating admin wallet...");

				let _wallet = {
					name: `${config.admin.username}-tr-wallet`,
					password: config.admin.password,
					firstAddress: ""
				};

				client.createNewWallet(
					_wallet.name,
					_wallet.password,
					(err, wallet, backupInfo) => {
						logger.warn("Admin wallet created!");
						// initialize wallet with an address
						logger.warn("Initializing admin wallet with new address...");
						wallet.getNewAddress((err, address) => {
							logger.warn("Admin wallet address initialized!");
							_wallet.firstAddress = address;
							callback(null, _wallet);
						});
					}
				);
			});

			// create user
			asyncTasks.push((wallet, callback) => {
				logger.warn("Creating admin...");
				let admin = new User({
					// fullName: "Administrator",
					email: config.admin.email,
					username: config.admin.username,
					// password: "admin1234",
					provider: "local",
					roles: [C.ROLE_ADMIN, C.ROLE_USER],
					verified: true,
					walletName: wallet.name,
					walletPassword: wallet.password,
					walletAddress: wallet.firstAddress
				});

				admin.save((err, admin) => {
					logger.warn("Admin created!");
					callback(err, admin);
				});
			});

			// setup wallet webhooks
			asyncTasks.push((admin, callback) => {
				logger.warn("Setting up admin wallet webhooks...");
				let webhookUrl = config.blocktrail.webhookUrl + admin.id + "?token=" + config.blocktrail.webhookToken;
				let wallet = client.initWallet(
					admin.walletName,
					admin.walletPassword,
					(err, _wallet) => {
						logger.error(err);
						_wallet.setupWebhook(webhookUrl);
						logger.warn("Admin wallet webhooks all set!");
						callback(err, admin);
					}
				);
			});

			async.waterfall(asyncTasks, (err, res) => {
				if (err) return logger.error(err);
				logger.debug("Seeding done!");
			});
		}
	});

	// return User.find({}).exec().then(docs => {
	// 	if (docs.length === 0) {
	// 		logger.warn("Load default Users to DB...");

	// 		let users = [];

	// 		let admin = new User({
	// 			// fullName: "Administrator",
	// 			email: "admin@traderemit.com",
	// 			username: "admin",
	// 			// password: "admin1234",
	// 			provider: "local",
	// 			roles: [C.ROLE_ADMIN, C.ROLE_USER],
	// 			verified: true
	// 		});
	// 		users.push(admin.save());

	// 		// let test = new User({
	// 		// 	fullName: "Test User",
	// 		// 	email: "test@boilerplate-app.com",
	// 		// 	username: "test",
	// 		// 	password: "test1234",
	// 		// 	provider: "local",
	// 		// 	roles: [C.ROLE_USER],
	// 		// 	verified: true,
	// 		// 	apiKey: tokgen()
	// 		// });			
	// 		// users.push(test.save());

	// 		return Promise.all(users)
	// 		.then(() => {
	// 			if (!config.isProductionMode()) {
	// 				// Create fake users
	// 				return Promise.all(_.times(10, () => {
	// 					let fakeUser = fakerator.entity.user();
	// 					let user = new User({
	// 						fullName: fakeUser.firstName + " " + fakeUser.lastName,
	// 						email: fakeUser.email,
	// 						username: fakeUser.userName,
	// 						password: fakeUser.password,
	// 						provider: "local",
	// 						roles: [C.ROLE_USER],
	// 						verified: true
	// 						//apiKey: tokgen()
	// 					});
	// 					users.push(user.save());					
	// 				}));
	// 			}				
	// 		})
	// 		.then(() => {
	// 			logger.warn("Default users created!");
	// 		});
	// 	}
	// }).catch((err) => {
	// 	logger.error(err);
	// }).then(() => {
	// 	return require("../applogic/libs/seed-db")();
	// }).then(() => {
	// 	logger.debug("Seeding done!");
	// });	
};
