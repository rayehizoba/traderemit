"use strict";

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");
let path = require("path");

let _ = require("lodash");
let blocktrailClient = require("../../../core/blocktrailClient");

let Trade = require("./models/trade");
let User = require("../persons/models/user");
let Notification = require("../notifications/models/notification");

let moment = require("moment");
let async = require("async");
let blocktrail = require("blocktrail-sdk");
let Nexmo = require("nexmo");

let Sockets = require("../../../core/sockets");
let mailer = require("../../../libs/mailer");

module.exports = {
	settings: {
		name: "trades",
		version: 1,
		namespace: "trades",
		rest: true,
		ws: true,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Trade
	},

	actions: {
		find: {
			handler(ctx) {
				if (!ctx.params.code) return false;

				let id = Trade.schema.methods.decodeID(ctx.params.code);
				return Trade.findById(id, (err, trade) => {
					if (err) return logger.error(err);

					// if already expired, return trade
					if (trade.completed)
						return this.toJSON(trade);

					// if now is before expiry time, return trade
					if (moment().isBefore(trade.expiresAt) || trade.paymentSent)
						return this.toJSON(trade);

					let asyncTasks = {};

					// this trade is expired, update as such
					asyncTasks.trade = callback => {
						trade.completed = true;
						trade.summary = C.TRADE_SUMMARY_EXPIRED;
						trade.save((err, trade) => {
							if (err) return callback(err);
							callback(null, trade);
						});
					};

					// return sellers' escrowed balance to main balance
					asyncTasks.seller = callback => {
						let conditions = { username: trade.sellerUsername };
						User.findOne(conditions, (err, user) => {
							if (err) return callback(err);

							user.escrowedBalance -= trade.sellerCoinAmount;
							user.save((err, user) => {
								if (err) return callback(err);
								callback();
							});
						});
					};

					return async.parallel(asyncTasks, (err, results) => {
						if (err) return logger.error(err);
						return this.toJSON(results.trade);
					});
				});
			}
		},

		update: {
			handler(ctx) {
				if (!ctx.params.code) return false;
				if (!ctx.params.type) return false;

				// get trade
				let id = Trade.schema.methods.decodeID(ctx.params.code);
				return Trade.findById(id, (err, trade) => {
					if (err) return logger.error(err);

					//
					// update this trade as paid
					//
					if (ctx.params.type === "paid") {
						// assert this user is buyer
						if (trade.buyerUsername !== ctx.user.username) return false;

						trade.paymentSent = true;
						trade.steps += 1;
						return trade.save((err, trade) => {
							if (err) return logger.error(err);

							let asyncSubTasks = [];

							let notification = new Notification({
								username: trade.sellerUsername,
								tradeId: trade.id,
								title: `Your trade with ${trade.buyerUsername} #${trade.code} has been marked as paid`,
								type: C.TRADE_NOTIF_PAID
							});
							notification.save();

							notification = new Notification({
								username: trade.buyerUsername,
								tradeId: trade.id,
								title: `Your trade with ${trade.sellerUsername} #${trade.code} has been marked as paid`,
								type: C.TRADE_NOTIF_PAID
							});
							notification.save();

							// new email notification for offline user
							let targetUsername;
							let tradePartnerUsername;

							if (ctx.user.username === trade.buyerUsername) {
								targetUsername = trade.sellerUsername;
								tradePartnerUsername = trade.buyerUsername;
							} else {
								targetUsername = trade.buyerUsername;
								tradePartnerUsername = trade.sellerUsername;
							}

							// check if targetUser is online
							let isOnline = Sockets.isOnline({ username: targetUsername });

							if (!isOnline) {
								// get this user
								let conditions = { username: targetUsername };
								User.findOne(conditions).exec()
									.then(user => {
										ctx.res.render("mail/updatedTrade", {
											name: user.username,
											tradePartnerName: tradePartnerUsername,
											status: 'PAID',
											code: trade.code
										}, (err, html) => {
											if (err)
												return logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);

											let subject = `Your trade with ${tradePartnerUsername} #${trade.code} has been marked as paid`;
											mailer.send(user.email, subject, html, (err, info) => {
												if (err)
													logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);
											});
										});
									});
							}

							return true;
						});
					}

					//
					// update this trade as released
					//
					if (ctx.params.type === "release") {
						// assert this user is seller
						if (trade.sellerUsername !== ctx.user.username) return false;

						// assert this trade is not completed already
						if (trade.completed) return false;

						let asyncTasks = [];

						// Send TradeRemit trading fee to TradeRemit wallet
						//
						// find admin account
						asyncTasks.push(callback => {
							let conditions = { username: config.admin.username };
							User.findOne(conditions, (err, admin) => {
								if (err) return callback(err);
								callback(null, admin);
							});
						});

						// send fee from seller wallet to admin wallet
						asyncTasks.push((admin, callback) => {
							let forceFees = this.getForceFees(trade.buyerCoinAmount);
							let totalFee = trade.sellerCoinAmount - trade.buyerCoinAmount;
							let receiveAddress = admin.walletAddress;
							let client = blocktrailClient.new();

							// get the seller
							let conditions = { username: trade.sellerUsername };
							User.findOne(conditions, (err, seller) => {
								if (err) return callback(err);

								client.initWallet(seller.walletName, seller.walletPassword,
									(err, wallet) => {
										if (err) return callback(err);

										let tx = new Object();
										tx[receiveAddress] = totalFee - forceFees.toAdmin - forceFees.toBuyer;
										let options = {
											forcefee: forceFees.toAdmin
										};

										wallet.pay(tx, null, false, true,
											blocktrail.Wallet.FEE_STRATEGY_FORCE_FEE,
											null, options,
											(err, result) => {
												// if an error occurs, this is probably
												// because the fee is too small and considered "dust"
												// this should not cause the transaction not to proceed
												if (err)
													logger.error(`Error sending fee to admin wallet: ${err}`);
												else {
													logger.debug("Trade fee sent to admin wallet!");
													logger.debug(result);
												}
												return callback(null, seller, client, forceFees);
											});
									});
							});
						});

						// send btc to buyer wallet
						asyncTasks.push((seller, client, forceFees, callback) => {
							client.initWallet(seller.walletName, seller.walletPassword,
								(err, wallet) => {
									if (err) return callback(err);

									let tx = new Object();
									tx[trade.buyerAddress] = trade.buyerCoinAmount;
									let options = {
										forcefee: forceFees.toBuyer
									};

									wallet.pay(tx, null, true, true,
										blocktrail.Wallet.FEE_STRATEGY_FORCE_FEE,
										null, options,
										(err, result) => {
											if (err) return callback(err);
											logger.debug("Coin sent from seller's wallet!");
											logger.debug(result);
											callback(null, seller);
										});
								});
						});

						// decrement seller's escrowed balance
						asyncTasks.push((seller, callback) => {
							seller.escrowedBalance -= trade.sellerCoinAmount;
							seller.save((err, seller) => {
								if (err) return callback(err);
								callback();
							});
						});

						// update trade as completed
						// update trade summary as released
						// update trade steps
						asyncTasks.push(callback => {
							trade.completed = true;
							trade.summary = C.TRADE_SUMMARY_RELEASED;
							trade.steps = 4;
							trade.save((err, updatedTrade) => {
								if (err) return callback(err);

								let conditions = {
									completed: true,
									$and: [
										{ buyerUsername: updatedTrade.buyerUsername },
										{ sellerUsername: updatedTrade.sellerUsername }
									]
								};
								Trade.count(conditions, (err, count) => {
									let isDistinctTrade = false;
									if (count <= 1) isDistinctTrade = true;
									callback(null, updatedTrade, isDistinctTrade);
								});
							});
						});

						// update trade partners reputation
						asyncTasks.push((updatedTrade, isDistinctTrade, callback) => {
							logger.debug("updating traders reputation...");
							// buyer reputation
							let conditions = { username: updatedTrade.buyerUsername };
							User.findOne(conditions, (err, user) => {
								user.totalBtcTraded += updatedTrade.buyerCoinAmount;
								user.totalSuccessfulTrades += 1;
								user.totalBuys += 1;
								user.summonsLeft += 1;
								if (isDistinctTrade)
									user.totalDistinctPartners += 1;

								user.save();
							});

							// seller reputation
							conditions = { username: updatedTrade.sellerUsername };
							User.findOne(conditions, (err, user) => {
								user.totalBtcTraded += updatedTrade.sellerCoinAmount;
								user.totalSuccessfulTrades += 1;
								user.summonsLeft += 1;
								// update seller time score here
								if (isDistinctTrade)
									user.totalDistinctPartners += 1;

								user.save();
							});

							callback(null, updatedTrade);
						});

						// create a new notification
						asyncTasks.push((updatedTrade, callback) => {
							let notification = new Notification({
								username: trade.sellerUsername,
								tradeId: trade.id,
								title: `Your trade with ${trade.buyerUsername} #${trade.code} has been marked as released`,
								type: C.TRADE_NOTIF_RELEASED
							});
							notification.save();

							notification = new Notification({
								username: trade.buyerUsername,
								tradeId: trade.id,
								title: `Your trade with ${trade.sellerUsername} #${trade.code} has been marked as released`,
								type: C.TRADE_NOTIF_RELEASED
							});
							notification.save();

							// new email notification for offline user
							let targetUsername;
							let tradePartnerUsername;

							if (ctx.user.username === trade.buyerUsername) {
								targetUsername = trade.sellerUsername;
								tradePartnerUsername = trade.buyerUsername;
							} else {
								targetUsername = trade.buyerUsername;
								tradePartnerUsername = trade.sellerUsername;
							}

							// check if targetUser is online
							let isOnline = Sockets.isOnline({ username: targetUsername });

							if (!isOnline) {
								// get this user
								let conditions = { username: targetUsername };
								User.findOne(conditions).exec()
									.then(user => {
										ctx.res.render("mail/updatedTrade", {
											name: user.username,
											tradePartnerName: tradePartnerUsername,
											status: 'RELEASED',
											code: trade.code
										}, (err, html) => {
											if (err)
												return logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);

											let subject = `Your trade with ${tradePartnerUsername} #${trade.code} has been marked as released`;
											mailer.send(user.email, subject, html, (err, info) => {
												if (err)
													logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);
											});
										});
									});
							}

							callback();
						});

						// return trade
						return async.waterfall(asyncTasks, (err, updatedTrade) => {
							if (err) return logger.error(err);
							return this.toJSON(updatedTrade);
						});
					}

					//
					// update this trade as under dispute
					//
					if (ctx.params.type === "dispute") {
						// assert this user is buyer or seller
						let userIsSeller = ctx.user.username === trade.sellerUsername;
						let userIsBuyer = ctx.user.username === trade.buyerUsername;
						if (!userIsBuyer && !userIsSeller) return false;

						// assert this trade is not already under dispute
						if (trade.underDispute) return this.toJSON(trade);

						trade.underDispute = true;
						return trade.save((err, trade) => {
							if (err) return logger.error(err);

							// notify trade partners of trade update
							let notification = new Notification({
								username: trade.sellerUsername,
								tradeId: trade.id,
								title: `Your trade with ${trade.buyerUsername} #${trade.code} is under dispute`,
								type: C.TRADE_NOTIF_UNDER_DISPUTE
							});
							notification.save();

							notification = new Notification({
								username: trade.buyerUsername,
								tradeId: trade.id,
								title: `Your trade with ${trade.sellerUsername} #${trade.code} is under dispute`,
								type: C.TRADE_NOTIF_UNDER_DISPUTE
							});
							notification.save();

							// new email notification for offline user
							let targetUsername;
							let tradePartnerUsername;

							if (ctx.user.username === trade.buyerUsername) {
								targetUsername = trade.sellerUsername;
								tradePartnerUsername = trade.buyerUsername;
							} else {
								targetUsername = trade.buyerUsername;
								tradePartnerUsername = trade.sellerUsername;
							}

							// check if targetUser is online
							let isOnline = Sockets.isOnline({ username: targetUsername });

							if (!isOnline) {
								// get this user
								let conditions = { username: targetUsername };
								User.findOne(conditions).exec()
									.then(user => {
										ctx.res.render("mail/updatedTrade", {
											name: user.username,
											tradePartnerName: tradePartnerUsername,
											status: 'UNDER DISPUTE',
											code: trade.code
										}, (err, html) => {
											if (err)
												return logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);

											let subject = `Your trade with ${tradePartnerUsername} #${trade.code} is under dispute`;
											mailer.send(user.email, subject, html, (err, info) => {
												if (err)
													logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);
											});
										});
									});
							}

							return true;
						});
					}

					//
					// update this trade as cancelled
					//
					if (ctx.params.type === "cancel") {
						// assert this user is buyer
						let userIsBuyer = ctx.user.username === trade.buyerUsername;
						if (!userIsBuyer) return;

						// assert this trade is not already cancelled
						if (trade.completed) return this.toJSON(trade);

						let asyncTasks = [];

						// update trade
						asyncTasks.push(callback => {
							trade.completed = true;
							trade.summary = C.TRADE_SUMMARY_CANCELLED;

							// save changes to trade
							// create and send notifications
							trade.save((err, trade) => {
								if (err) return callback(err);

								// notify trade partners of trade update
								let notification = new Notification({
									username: trade.sellerUsername,
									tradeId: trade.id,
									title: `Your trade with ${trade.buyerUsername} #${trade.code} has been cancelled`,
									type: C.TRADE_NOTIF_CANCELLED
								});
								notification.save();

								notification = new Notification({
									username: trade.buyerUsername,
									tradeId: trade.id,
									title: `Your trade with ${trade.sellerUsername} #${trade.code} has been cancelled`,
									type: C.TRADE_NOTIF_CANCELLED
								});
								notification.save();

								// new email notification for offline user
								let targetUsername;
								let tradePartnerUsername;

								if (ctx.user.username === trade.buyerUsername) {
									targetUsername = trade.sellerUsername;
									tradePartnerUsername = trade.buyerUsername;
								} else {
									targetUsername = trade.buyerUsername;
									tradePartnerUsername = trade.sellerUsername;
								}

								// check if targetUser is online
								let isOnline = Sockets.isOnline({ username: targetUsername });

								if (!isOnline) {
									// get this user
									let conditions = { username: targetUsername };
									User.findOne(conditions).exec()
										.then(user => {
											ctx.res.render("mail/updatedTrade", {
												name: user.username,
												tradePartnerName: tradePartnerUsername,
												status: 'CANCELLED',
												code: trade.code
											}, (err, html) => {
												if (err)
													return logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);

												let subject = `Your trade with ${tradePartnerUsername} #${trade.code} has been cancelled`;
												mailer.send(user.email, subject, html, (err, info) => {
													if (err)
														logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);
												});
											});
										});
								}

								callback(null, trade);
							});
						});

						// update buyer reputation and
						// return sellers' escrowed balance to main balance
						asyncTasks.push((trade, callback) => {
							let asyncSubTasks = [];

							// update buyer reputation
							asyncSubTasks.push(callback => {
								// get buyer
								let conditions = { username: trade.buyerUsername };
								User.findOne(conditions).exec()
									.then(user => {
										user.totalCancelledTrades += 1;
										user.save((err, user) => {
											if (err) return callback(err);
											callback(null, user);
										});
									});
							});

							// return sellers' escrowed balance to main balance
							asyncSubTasks.push(callback => {
								let conditions = { username: trade.sellerUsername };
								User.findOne(conditions, (err, user) => {
									if (err) return callback(err);

									user.escrowedBalance -= trade.sellerCoinAmount;
									user.save((err, user) => {
										if (err) return callback(err);
										callback();
									});
								});
							});

							async.parallel(asyncSubTasks, err => {
								if (err) return callback(err);
								callback();
							});
						});

						return async.waterfall(asyncTasks, err => {
							if (err) return logger.error(err);
							return true;
						});
					}
				});
			}
		},

		all: {
			handler(ctx) {
				let filter = {};
				filter.$or = [
					{ buyerUsername: ctx.user.username },
					{ sellerUsername: ctx.user.username }
				];

				let asyncTasks = [];

				// update expired trades
				asyncTasks.push(callback => {
					let now = moment();
					let conditions = {
						expiresAt: { $lte: now },
						completed: false,
						paymentSent: false,
						$or: [
							{ buyerUsername: ctx.user.username },
							{ sellerUsername: ctx.user.username }
						]
					};

					// find expired trades
					Trade.find(conditions, (err, trades) => {
						if (err) return callback(err);

						let update = {
							completed: true,
							summary: C.TRADE_SUMMARY_EXPIRED
						};
						_.forEach(trades, trade => {
							// update each expired trade
							Trade.findByIdAndUpdate(trade.id, update).exec()
								.then(trade => {
									let conditions = { username: trade.sellerUsername };
									User.findOne(conditions).exec().then(user => {
										// return sellers' escrowed balance to main balance
										user.escrowedBalance -= trade.sellerCoinAmount;
										user.save(err => {
											if (err) return callback(err);
										});
									});
								});
						});

						callback();
					});
				});

				// find open and closed trades
				asyncTasks.push(callback => {
					let trades = {
						open: [],
						closed: []
					};

					async.parallel([
						// find open trades
						callback => {
							filter.completed = false;
							Trade.find(filter).sort("-updatedAt").exec()
								.then(docs => {
									trades.open = this.toJSON(docs);
									callback();
								});
						},
						// find closed trades
						callback => {
							filter.completed = true;
							Trade.find(filter).sort("-updatedAt").exec()
								.then(docs => {
									trades.closed = this.toJSON(docs);
									callback();
								});
						}
					], err => {
						if (err) return logger.error(err);
						callback(null, trades);
					});
				});

				return new Promise((resolve, reject) => {
					async.series(asyncTasks, (err, res) => {
						if (err) return logger.error(err);
						resolve(res[1]);
					});
				});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let isBuyAd = ctx.params.adType === "buy";

				// buyer coin amount = AP/(P+F)
				// seller coin amount = (A(P+F))/P
				// where
				// A = seller coin amount if this is a buy ad
				// A = buyer coin amount if this is a sell ad
				// P = price set for trade
				// F = TradeRemit trading fee
				let A = ctx.params.coinAmount; // in btc
				let P = ctx.params.sellerPrice;
				let F = config.tradingFee;

				// for a sell ad
				// cash transfer amount = AR(P+F)
				// for a buy ad
				// cash transfer amount = AR(P-F)
				// where
				// R = BTC market rate ATM
				let R = ctx.params.bitUSDRate;
				let transferAmount = isBuyAd ? A * R * (P - F) : A * R * (P + F);

				let sellerCoinAmount, buyerCoinAmount;
				if (isBuyAd) {
					// me is seller
					sellerCoinAmount = blocktrail.toSatoshi(ctx.params.coinAmount);
					buyerCoinAmount = A * P / (P + F);
					buyerCoinAmount = blocktrail.toSatoshi(buyerCoinAmount);
				} else {
					// me is buyer
					buyerCoinAmount = blocktrail.toSatoshi(ctx.params.coinAmount);
					sellerCoinAmount = (A * (P + F)) / P;
					sellerCoinAmount = blocktrail.toSatoshi(sellerCoinAmount);
				}

				let asyncTasks = [];

				// run seller wallet balance check
				// secure bitcoins if passed
				asyncTasks.push(callback => {
					// get seller
					let conditions = { username: ctx.params.sellerUsername };
					User.findOne(conditions).exec()
						.then(user => {
							if (buyerCoinAmount > user.computedBalance)
								return callback(true, "Seller does not have enough Bitcoin \
							in wallet to start trade!");

							user.escrowedBalance += sellerCoinAmount;
							user.save((err, user) => {
								if (err) return callback(err);
								callback();
							});
						});
				});

				// create new trade
				asyncTasks.push(callback => {
					let trade = new Trade({
						buyerUsername: ctx.params.buyerUsername,
						buyerAddress: ctx.params.buyerAddress,
						sellerUsername: ctx.params.sellerUsername,
						buyerCoinAmount: buyerCoinAmount,
						sellerCoinAmount: sellerCoinAmount,
						transferAmount: transferAmount,
						sellerPrice: ctx.params.sellerPrice,
						bitUSDRate: ctx.params.bitUSDRate,
						expiresAt: moment().add(ctx.params.expiresAt, "m"),
						sellerBankName: ctx.params.sellerBankName,
						sellerBankAccountName: ctx.params.sellerBankAccountName,
						sellerBankAccountNumber: ctx.params.sellerBankAccountNumber,
					});

					// save trade
					trade.save((err, trade) => {
						if (err) return callback(err);
						callback(null, trade);
					});
				});

				// add and send notification of new open trade
				// and send email notification to user if offline
				asyncTasks.push((trade, callback) => {
					let asyncSubTasks = [];

					// new notifications
					asyncSubTasks.push(done => {
						let notification = new Notification({
							username: trade.sellerUsername,
							tradeId: trade.id,
							title: `You have a new trade #${trade.code} with ${trade.buyerUsername}`,
							type: C.TRADE_NOTIF_NEW_TRADE
						});
						notification.save();

						notification = new Notification({
							username: trade.buyerUsername,
							tradeId: trade.id,
							title: `You have a new trade #${trade.code} with ${trade.sellerUsername}`,
							type: C.TRADE_NOTIF_NEW_TRADE
						});
						notification.save();

						done();
					});

					// new email notification for offline user
					asyncSubTasks.push(done => {
						let targetUsername;
						let tradePartnerUsername;

						if (ctx.user.username === trade.buyerUsername) {
							targetUsername = trade.sellerUsername;
							tradePartnerUsername = trade.buyerUsername;
						} else {
							targetUsername = trade.buyerUsername;
							tradePartnerUsername = trade.sellerUsername;
						}

						// check if targetUser is online
						let isOnline = Sockets.isOnline({ username: targetUsername });

						if (!isOnline) {
							// get this user
							let conditions = { username: targetUsername };
							User.findOne(conditions).exec()
								.then(user => {
									ctx.res.render("mail/newTrade", {
										name: user.username,
										tradePartnerName: tradePartnerUsername,
										code: trade.code
									}, (err, html) => {
										if (err)
											return logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);

										let subject = `You have a new trade #${trade.code} on TradeRemit`;
										mailer.send(user.email, subject, html, (err, info) => {
											if (err)
												logger.error(`Could not notify offline user (${targetUsername}) of new trade: ${err}`);
										});
									});
								});
						}

						done();
					});

					async.parallel(asyncSubTasks, err => {
						if (err) return callback(err);
						callback(null, trade);
					});
				});

				return new Promise((resolve, reject) => {
					async.waterfall(asyncTasks, (err, res) => {
						if (err) {
							logger.error(err);
							resolve({ error: res });
						}
						resolve(res);
					});
				});
			}
		},

		summon: {
			handler(ctx) {
				// request must have tradeCode and username to summon
				let tradeCode = ctx.params.tradeCode;
				let username = ctx.params.username;
				if (!tradeCode && !username) return;

				// if user has run out of summons, cancel request
				if (ctx.user.summonsLeft === 0) return;

				let asyncTasks = [];

				// fetch trade
				asyncTasks.push(callback => {
					let id = Trade.schema.methods.decodeID(tradeCode);
					return Trade.findById(id, (err, trade) => {
						if (err) return callback(err);

						callback(null, trade);
					});
				});

				// this user must be either the buyer or the seller
				asyncTasks.push((trade, callback) => {
					if (
						(trade.buyerUsername !== ctx.user.username) &&
						(trade.sellerUsername !== ctx.user.username))
						return callback(`This request does not originate from \
						trade buyer or seller. Username => ${ctx.user.username}`);

					callback(null, trade);
				});

				// find user phone to call
				asyncTasks.push((trade, callback) => {
					let conditions = { username };
					User.findOne(conditions, (err, user) => {
						if (err) return callback(err);

						if (!user.phoneVerified)
							return callback(true, {
								error: `Sorry, ${username}'s phone has not verified`
							});

						let phone = `234${user.phone}`;
						callback(null, trade, phone);
					});
				});

				// initiate call
				asyncTasks.push((trade, phone, callback) => {
					let nexmo = new Nexmo({
						apiKey: config.nexmo.apiKey,
						apiSecret: config.nexmo.apiSecret,
						applicationId: config.nexmo.applicationId,
						privateKey: path.join(__dirname, "..", "..", "..", "..", "nexmo_app_private.key")
					});

					nexmo.calls.create({
						to: [{
							type: "phone",
							number: phone
						}],
						from: {
							type: "phone",
							number: "2348143545665"
						},
						answer_url: [config.nexmo.answerUrl]
					}, err => {
						if (err) return callback(true, {
							error: `Sorry, could not place a call to ${username}. Try again.`
						});
						callback(null, trade);
					});
				});

				// update nextSummonTime
				asyncTasks.push((trade, callback) => {
					// 5 minutes into the future
					if (trade.buyerUsername === ctx.user.username)
						trade.buyerNextSummonTime = moment().add(5, "m");
					else
						trade.sellerNextSummonTime = moment().add(5, "m");

					trade.save(err => {
						if (err) return callback(err);
						callback(null, trade);
					});
				});

				// reduce summons available
				asyncTasks.push((trade, callback) => {
					let conditions = { username: ctx.user.username };
					User.findOne(conditions).exec()
						.then(doc => {
							doc.summonsLeft -= 1;
							doc.save();
						});
					callback(null, trade);
				});

				return new Promise((resolve, reject) => {
					async.waterfall(asyncTasks, (err, res) => {
						if (err) logger.error(err);
						resolve(res);
					});
				});
			}
		}

	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("adType"))
				ctx.validateParam("adType").notEmpty(ctx.t("app:TradeAdTypeCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("buyerUsername"))
				ctx.validateParam("buyerUsername").trim().notEmpty(ctx.t("app:TradeBuyerUsernameCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("buyerAddress"))
				ctx.validateParam("buyerAddress").trim().notEmpty(ctx.t("app:TradeBuyerAddressCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("sellerUsername"))
				ctx.validateParam("sellerUsername").trim().notEmpty(ctx.t("app:TradeSellerUsernameCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("sellerBankName"))
				ctx.validateParam("sellerBankName").trim().notEmpty(ctx.t("app:TradeSellerBankNameCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("sellerBankAccountName"))
				ctx.validateParam("sellerBankAccountName").trim().notEmpty(ctx.t("app:TradeSellerBankAccountNameCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("sellerBankAccountNumber"))
				ctx.validateParam("sellerBankAccountNumber").trim().notEmpty(ctx.t("app:TradeSellerBankAccountNumberCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("coinAmount"))
				ctx.validateParam("coinAmount").notEmpty(ctx.t("app:TradeCoinAmountCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("sellerPrice"))
				ctx.validateParam("sellerPrice").notEmpty(ctx.t("app:TradeSellerPriceCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("bitUSDRate"))
				ctx.validateParam("bitUSDRate").notEmpty(ctx.t("app:TradeBitUSDRateCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("expiresAt"))
				ctx.validateParam("expiresAt").notEmpty(ctx.t("app:TradeExpiresAtCannotBeEmpty")).end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		},

		/**
		 * Get force fees based on transaction amount to be performed
		 * we will use this to determine exactly how much our
		 * blockchain transactions will cost
		 * 
		 * @param  {number} txAmount		transaction amount in satoshi
		 * @returns {Object} force fees to transaction to admin wallet and to buyer wallet in satoshi
		 */
		getForceFees(txAmount) {
			let forceFees = {};
			forceFees.toAdmin = blocktrail.toSatoshi(0.00004);

			txAmount = blocktrail.toBTC(txAmount);

			if (txAmount > 0.7)
				forceFees.toBuyer = blocktrail.toSatoshi(0.0002);

			else if (txAmount > 0.5)
				forceFees.toBuyer = blocktrail.toSatoshi(0.0001);

			else if (txAmount >= 0.4)
				forceFees.toBuyer = blocktrail.toSatoshi(0.00009);

			else if (txAmount >= 0.3)
				forceFees.toBuyer = blocktrail.toSatoshi(0.00008);

			else if (txAmount >= 0.1)
				forceFees.toBuyer = blocktrail.toSatoshi(0.00007);

			else
				forceFees.toBuyer = blocktrail.toSatoshi(0.00004);

			return forceFees;
		}

	},

	/**
	 * Check the owner of model
	 * 
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	// ownerChecker(ctx) {
	// 	return new Promise((resolve, reject) => {
	// 		// ctx.assertModelIsExist(ctx.t("app:TradeNotFound"));

	// 		let userIsBuyer = ctx.model.buyerUsername === ctx.user.username;
	// 		let userIsSeller = ctx.model.sellerUsername === ctx.user.username;
	// 		if (userIsBuyer || userIsSeller || ctx.isAdmin()) 
	// 			resolve();
	// 		else
	// 			reject();
	// 	});
	// },

	init(ctx) { },

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	// graphql: {

	// 	query: `
	// 		posts(limit: Int, offset: Int, sort: String): [Post]
	// 		post(code: String): Post
	// 	`,

	// 	types: `
	// 		type Post {
	// 			code: String!
	// 			title: String
	// 			content: String
	// 			author: Person!
	// 			views: Int
	// 			votes: Int,
	// 			voters(limit: Int, offset: Int, sort: String): [Person]
	// 			createdAt: Timestamp
	// 			createdAt: Timestamp
	// 		}
	// 	`,

	// 	mutation: `
	// 		postCreate(title: String!, content: String!): Post
	// 		postUpdate(code: String!, title: String, content: String): Post
	// 		postRemove(code: String!): Post

	// 		postVote(code: String!): Post
	// 		postUnvote(code: String!): Post
	// 	`,

	// 	resolvers: {
	// 		Query: {
	// 			posts: "find",
	// 			post: "get"
	// 		},

	// 		Mutation: {
	// 			postCreate: "create",
	// 			postUpdate: "update",
	// 			postRemove: "remove",
	// 			postVote: "vote",
	// 			postUnvote: "unvote"
	// 		}
	// 	}
	// }

};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
