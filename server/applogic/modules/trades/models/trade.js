"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("trades");
let autoIncrement 	= require("mongoose-auto-increment");
let Sockets = require("../../../../core/sockets");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let TradeSchema = new Schema({
	buyerUsername: {
		type: String,
		trim: true,
		required: "Please fill in the buyer's username",
		ref: "User"
	},
	buyerAddress: {
		type: String,
		required: "Please fill in the buyer's wallet address",
		trim: true
	},
	sellerUsername: {
		type: String,
		trim: true,
		required: "Please fill in the seller's username",
		ref: "User"
	},
	sellerBankName: {
		type: String,
		trim: true,
		required: "Please fill in the seller's bank name",
	},
	sellerBankAccountName: {
		type: String,
		trim: true,
		required: "Please fill in the seller's bank account name",
	},
	sellerBankAccountNumber: {
		type: String,
		trim: true,
		required: "Please fill in the seller's bank account number",
	},
	buyerCoinAmount: {
		type: Number,
		required: "Please fill in the buyer coin amount"
	},
	sellerCoinAmount: {
		type: Number,
		required: "Please fill in the seller coin amount"
	},
	transferAmount: {
		type: Number,
		required: "Please fill in the cash transfer amount"
	},
	sellerPrice: {
		type: Number,
		required: "Please fill in the price set by seller for advertisement"
	},
	bitUSDRate: {
		type: Number,
		required: "Please fill in the current bitUSD rate"
	},
	steps: {
		type: Number,
		default: 2
	},
	expiresAt: {
		type: Date,
		required: "Please set time for this trade to expire"
	},
	paymentSent: {
		type: Boolean,
		default: false
	},
	buyerNextSummonTime: {
		type: Date
	},
	sellerNextSummonTime: {
		type: Date
	},
	underDispute: {
		type: Boolean,
		default: false
	},
	summary: {
		type: String
	},
	completed: {
		type: Boolean,
		default: false
	},
	metadata: {}

}, schemaOptions);

TradeSchema.virtual("code").get(function() {
	return this.encodeID();
});

TradeSchema.plugin(autoIncrement.plugin, {
	model: "Trade",
	startAt: 1
});

/**
 * @param tradeId // optional parameter, used by the message schema to create its tradeCode virtual
 */
TradeSchema.methods.encodeID = function(tradeId) {
	if (tradeId) return hashids.encodeHex(tradeId);
	return hashids.encodeHex(this._id);
};

TradeSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

TradeSchema.pre("save", function (next) {
	this.modified = {};

	if (this.isNew) {
		this.wasNew = true;
		return next();
	}

	if (this.isModified("paymentSent"))
		this.modified.paymentSent = true;
	
	if (this.isModified("underDispute"))
		this.modified.underDispute = true;

	if (this.isModified("completed")) 
		this.modified.completed = true;

	next();
});

TradeSchema.post("save", function (trade) {
	let seller = { username: trade.sellerUsername };
	let buyer = { username: trade.buyerUsername };

	let cmd = "";
	if (this.wasNew) cmd = "newOpenTrade";
	else cmd = "tradeUpdated";
	
	Sockets.emitUser(seller, cmd, this.toJSON(trade));
	Sockets.emitUser(buyer, cmd, this.toJSON(trade));
	// if (this.modified.paymentSent) {
	// 	Sockets.emitUser(seller, "tradeUpdated", this.toJSON(trade));
	// 	Sockets.emitUser(buyer, "tradeUpdated", this.toJSON(trade));
	// }
	// if (this.modified.underDispute) {
	// 	Sockets.emitUser(seller, "tradeUpdated", this.toJSON(trade));
	// 	Sockets.emitUser(buyer, "tradeUpdated", this.toJSON(trade));
	// }
	// if (this.modified.completed) {
	// 	Sockets.emitUser(seller, "tradeUpdated", this.toJSON(trade));
	// 	Sockets.emitUser(buyer, "tradeUpdated", this.toJSON(trade));
	// }
});


/*
TradeSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Trade = mongoose.model("Trade", TradeSchema);

module.exports = Trade;
