"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("messages");
let autoIncrement 	= require("mongoose-auto-increment");
let Trade = require("../../trades/models/trade");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let MessageSchema = new Schema({
	tradeId: {
		type: Number,
		required: "Please this message must reference a trade.",
		ref: "Trade"
	},
	body: {
		type: String,
		trim: true
	},
	senderUsername: {
		type: String,
		trim: true,
		ref: "User"
	},
	receiverUsername: {
		type: String,
		trim: true,
		ref: "User"
	},
	isImage: {
		type: Boolean,
		default: false
	},
	metadata: {}

}, schemaOptions);

MessageSchema.virtual("code").get(function() {
	return this.encodeID();
});

MessageSchema.virtual("tradeCode").get(function() {
	return Trade.schema.methods.encodeID(this.tradeId);
});

MessageSchema.plugin(autoIncrement.plugin, {
	model: "Message",
	startAt: 1
});

MessageSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

MessageSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

/*
PostSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Message = mongoose.model("Message", MessageSchema);

module.exports = Message;
