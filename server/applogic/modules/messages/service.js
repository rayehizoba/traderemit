"use strict";
// modelled after posts services

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let async = require("async");
let _ = require("lodash");
let cloudinary = require('cloudinary');

let Message = require("./models/message");
let Trade = require("../trades/models/trade");
let Notification = require("../notifications/models/notification");
let Sockets = require("../../../core/sockets");

module.exports = {
	settings: {
		name: "messages",
		version: 1,
		namespace: "messages",
		rest: true,
		ws: true,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Message,
	},

	actions: {
		create: {
			handler(ctx) {
				// define a function to use later
				let sendNotifications = json => {
					let notification = new Notification({
						username: ctx.params.receiverUsername,
						tradeId: tradeId,
						title: `You have a new message from ${ctx.params.senderUsername} in trade #${tradeCode}`,
						type: C.TRADE_NOTIF_NEW_MESSAGE
					});

					let receiver = { username: ctx.params.receiverUsername };
					notification.save().then(notification => {
						Sockets.emitUser(receiver, "newNotification", this.toJSON(notification));
					});
					Sockets.emitUser(receiver, "newMessage", json);
					return json;
				};

				this.validateParams(ctx, true);

				let tradeCode = ctx.params.tradeCode;
				let tradeId = Trade.schema.methods.decodeID(tradeCode);

				if (ctx.params.isImage && (ctx.params.isImage === '1')) {
					return cloudinary.v2.uploader.upload(ctx.params.body,
						(error, result) => {
							if (error) return logger.error(error);

							let message = new Message({
								tradeId: tradeId,
								body: result.secure_url,
								senderUsername: ctx.params.senderUsername,
								receiverUsername: ctx.params.receiverUsername,
								isImage: true
							});
							return message.save()
								.then(doc => {
									return this.toJSON(doc);
								})
								.then(sendNotifications);
						});
				} else {
					let message = new Message({
						tradeId: tradeId,
						body: ctx.params.body,
						senderUsername: ctx.params.senderUsername,
						receiverUsername: ctx.params.receiverUsername
					});

					return message.save()
						.then(doc => {
							return this.toJSON(doc);
						})
						.then(sendNotifications);
				}
			}
		},

		// return all messages that match tradeId
		all: {
			handler(ctx) {
				let filter = {};

				if (!ctx.params.tradeCode) return;
				filter.tradeId = Trade.schema.methods.decodeID(ctx.params.tradeCode);
				// requesting user must be sender or receiver
				filter.$or = [
					{ senderUsername: ctx.user.username },
					{ receiverUsername: ctx.user.username }
				];

				return Message.find(filter).exec((err, docs) => {
					if (err) return logger.error(err);

					return this.toJSON(docs);
				});
			}
		},

	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("tradeCode"))
				ctx.validateParam("tradeCode").trim().notEmpty(ctx.t("app:MessageTradeCodeCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("body"))
				ctx.validateParam("body").trim().notEmpty(ctx.t("app:MessageBodyCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("senderUsername"))
				ctx.validateParam("senderUsername").trim().notEmpty(ctx.t("app:MessageSenderCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("receiverUsername"))
				ctx.validateParam("receiverUsername").trim().notEmpty(ctx.t("app:MessageReceiverCannotBeEmpty")).end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		}

	},

	init(ctx) {
		// register new sting function used in `create` action
		// String.prototype.replaceAll = function (search, replacement) {
		// 	let target = this;
		// 	return target.replace(new RegExp(search, "g"), replacement);
		// };
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
		`,

		types: `
		`,

		mutation: `
		`,

		resolvers: {
		}
	}

};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
