"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("reviews");
let autoIncrement 	= require("mongoose-auto-increment");
let Sockets = require("../../../../core/sockets");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let ReviewSchema = new Schema({
	summary: {
		type: String,
		required: "Please specify the review summary"
	},
	comment: {
		type: String,
		trim: true,
		required: "Please specify the review summary"
	},
	tradeId: {
		type: Number,
		required: "Please specify the trade id",
		ref: "Trade"
	},
	from: {
		type: String,
		required: "Please specify who the review is from"
	},
	to: {
		type: String,
		required: "Please specify who the review is to"
	},
	metadata: {}

}, schemaOptions);

ReviewSchema.virtual("code").get(function() {
	return this.encodeID();
});

ReviewSchema.plugin(autoIncrement.plugin, {
	model: "Review",
	startAt: 1
});

ReviewSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

ReviewSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

ReviewSchema.post("save", function(doc) {
	let user = { username: doc.to };
	Sockets.emitUser(user, "newReview", this.toJSON(doc));
});

let Review = mongoose.model("Review", ReviewSchema);

module.exports = Review;
