"use strict";
// modelled after posts services

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let async = require("async");
let _ = require("lodash");

let Review = require("./models/review");
let Trade = require("../trades/models/trade");
let Notification = require("../notifications/models/notification");

module.exports = {
	settings: {
		name: "reviews",
		version: 1,
		namespace: "reviews",
		rest: true,
		ws: true,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Review,
	},

	actions: {
		find: {
			handler(ctx) {
				let filter = {};

				if (!ctx.params.tradeCode) return;
				filter.tradeId = Trade.schema.methods.decodeID(ctx.params.tradeCode);

				return Review.find(filter).exec()
					.then(docs => {
						return this.toJSON(docs);
					});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let tradeId = Trade.schema.methods.decodeID(ctx.params.tradeCode);
				let review = new Review({
					summary: ctx.params.summary,
					comment: ctx.params.comment,
					tradeId: tradeId,
					from: ctx.params.from,
					to: ctx.params.to
				});

				return review.save()
					.then(doc => {
						return this.toJSON(doc);
					})
					.then(json => {
						let notificationType;
						switch (ctx.params.summary) {
							case "GOOD":
								notificationType = C.TRADE_NOTIF_REVIEW_GOOD;
								break;

							case "NEUTRAL":
								notificationType = C.TRADE_NOTIF_REVIEW_NEUTRAL;
								break;

							case "BAD":
								notificationType = C.TRADE_NOTIF_REVIEW_BAD;
						}
						// save a notification
						let notification = new Notification({
							username: ctx.params.to,
							tradeId: tradeId,
							title: `Your trade with ${ctx.params.from} #${ctx.params.tradeCode} has been reviewed as ${ctx.params.summary.toLowerCase()}`,
							type: notificationType
						});
						notification.save();
						return json;
					});
			}
		},

		all: {
			permission: C.PERM_PUBLIC,
			cache: true,
			handler(ctx) {
				let filter = {};
				if (ctx.params.to) filter.to = ctx.params.to;

				let query = Review.find(filter);
				let asyncTasks = {};

				// get requested reviews
				asyncTasks.data = callback => {
					ctx.queryPageSort(query).exec()
						.then(docs => {
							callback(null, docs);
						});
				};

				// get total reviews count
				asyncTasks.total = callback => {
					Review.count(filter, callback);
				};

				return new Promise((resolve, reject) => {
					async.series(asyncTasks, (err, results) => {
						if (err) return logger.error(err);
						resolve(results);
					});
				});
			}
		},

	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("from"))
				ctx.validateParam("from").trim().notEmpty(ctx.t("app:ReviewFromCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("to"))
				ctx.validateParam("to").trim().notEmpty(ctx.t("app:ReviewToCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("summary"))
				ctx.validateParam("summary").trim().notEmpty(ctx.t("app:ReviewSummaryCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("comment"))
				ctx.validateParam("comment").trim().notEmpty(ctx.t("app:ReviewCommentCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("tradeCode"))
				ctx.validateParam("tradeCode").trim().notEmpty(ctx.t("app:ReviewTradeCodeCannotBeEmpty")).end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		},

	},

	init(ctx) { },

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
		`,

		types: `
		`,

		mutation: `
		`,

		resolvers: {
		}
	}

};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
