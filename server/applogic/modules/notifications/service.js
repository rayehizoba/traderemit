"use strict";
// modelled after posts services

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");
let async = require("async");

let Notification = require("../notifications/models/notification");

module.exports = {
	settings: {
		name: "notifications",
		version: 1,
		namespace: "notifications",
		rest: true,
		ws: true,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Notification,
	},

	actions: {
		me: {
			handler(ctx) {
				let filter = { username: ctx.user.username };
				let query = Notification.find(filter);
				let asyncTasks = {};
	
				asyncTasks.data = callback => {
					ctx.queryPageSort(query).exec()
						.then(docs => {
							callback(null, docs);
						});
				};
	
				asyncTasks.total = callback => {
					Notification.count(filter, callback);
				};
	
				return new Promise((resolve, reject) => {
					async.series(asyncTasks, (err, results) => {
						if (err) return logger.error(err);
						resolve(results);
					});
				});
			}
		}
	},

	methods: {},

	/**
	 * Check the owner of model
	 *
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	// ownerChecker(ctx) {
	// 	return new Promise((resolve, reject) => {
	// 		ctx.assertModelIsExist(ctx.t("app:PostNotFound"));

	// 		if (ctx.model.username == ctx.user.username || ctx.isAdmin())
	// 			resolve();
	// 		else
	// 			reject();
	// 	});
	// },

	init(ctx) {
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
		`,

		types: `
		`,

		mutation: `
		`,

		resolvers: {
		}
	}

};
