"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("messages");
let autoIncrement 	= require("mongoose-auto-increment");
let Trade = require("../../trades/models/trade");
let Sockets = require("../../../../core/sockets");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let NotificationSchema = new Schema({
	username: {
		type: String,
		required: "Please this notification must reference a user.",
		ref: "User"
	},
	tradeId: {
		type: Number,
		required: "Please this notification must reference a trade.",
		ref: "Trade"
	},
	title: {
		type: String,
		trim: true,
		required: "Please enter the notification title"
	},
	read: {
		type: Boolean,
		default: false
	},
	type: {
		type: String,
		required: "Please specify this notification type"
	},
	metadata: {}

}, schemaOptions);

NotificationSchema.virtual("code").get(function() {
	return this.encodeID();
});

NotificationSchema.virtual("tradeCode").get(function() {
	return Trade.schema.methods.encodeID(this.tradeId);
});

NotificationSchema.plugin(autoIncrement.plugin, {
	model: "Notification",
	startAt: 1
});

NotificationSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

NotificationSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

NotificationSchema.post("save", function(doc) {
	let user = { username: doc.username };
	Sockets.emitUser(user, "newNotification", this.toJSON(doc));
});

let Notification = mongoose.model("Notification", NotificationSchema);

module.exports = Notification;
