"use strict";
// modelled after posts services

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");
let mailer = require("../../../libs/mailer");

let async = require("async");
let _ = require("lodash");

let Verification = require("./models/verification");
let User = require("../persons/models/user");

module.exports = {
	settings: {
		name: "verifications",
		version: 1,
		namespace: "verifications",
		rest: true,
		ws: false,
		graphql: false,
		permission: C.PERM_ADMIN,
		role: "admin",
		collection: Verification,
	},

	actions: {

		pending: {
			handler(ctx) {
				let filter = {};
				filter.idVerificationStatus = C.ID_VERIFICATION_STATUS_PENDING;

				let query = User.find(filter);
				let asyncTasks = {};

				// get users with pending verifications
				asyncTasks.data = callback => {
					ctx.queryPageSort(query).exec()
						.then(docs => {
							return this.toJSON(docs);
						})
						.then(users => {
							let populatedUsers = [];

							// attach verifications to each user
							async.each(users,
								(user, done) => {
									let conditions = { username: user.username };
									Verification.find(conditions).exec()
										.then(docs => {
											populatedUsers.push({
												user: user,
												verificationUploads: this.toJSON(docs)
											});
											done();
										});
								},
								err => {
									if (err) logger.error(err);
									callback(null, populatedUsers);
								});
						});
				};

				// get users with pending verifications total count
				asyncTasks.total = callback => {
					User.count(filter, callback);
				};

				return new Promise((resolve, reject) => {
					async.series(asyncTasks, (err, results) => {
						if (err) return logger.error(err);
						resolve(results);
					});
				});
			}
		},

		decline: {
			handler(ctx) {
				if (!ctx.params.code) return;
				
				let id = User.schema.methods.decodeID(ctx.params.code);
				let update = {
					idVerificationStatus: C.ID_VERIFICATION_STATUS_DECLINED
				};
				return User.findByIdAndUpdate(id, update).exec()
					.then(doc => {
						// send email to user
						if (!config.mailer.enabled) {
							const err = "Trying to send email without config.mailer not enabled; emailing skipped. Have you configured mailer yet?";
							logger.error(err);
						} else {
							let subject = `Your ID verification request is ${ C.ID_VERIFICATION_STATUS_DECLINED }`;
	
							ctx.res.render("mail/idVerificationStatus", {
								name: doc.username,
								status: C.ID_VERIFICATION_STATUS_DECLINED
							}, function (err, html) {
								if (err) return logger.error(err);
	
								mailer.send(config.admin.email, subject, html, function (err, info) {
									if (err) logger.error(`Unable to send Identity verification request status email to ${doc.username}`);
								});
							});
						}

						return this.toJSON(doc);
					});
			}
		},

		verify: {
			handler(ctx) {
				if (!ctx.params.code) return;
				
				let id = User.schema.methods.decodeID(ctx.params.code);
				let update = {
					idVerified: true,
					idVerificationStatus: C.ID_VERIFICATION_STATUS_VERIFIED
				};
				return User.findByIdAndUpdate(id, update).exec()
					.then(doc => {
						// send email to user
						if (!config.mailer.enabled) {
							const err = "Trying to send email without config.mailer not enabled; emailing skipped. Have you configured mailer yet?";
							logger.error(err);
						} else {
							let subject = `Your ID verification request is ${ C.ID_VERIFICATION_STATUS_DECLINED }`;
	
							ctx.res.render("mail/idVerificationStatus", {
								name: doc.username,
								status: C.ID_VERIFICATION_STATUS_DECLINED
							}, function (err, html) {
								if (err) return logger.error(err);
	
								mailer.send(config.admin.email, subject, html, function (err, info) {
									if (err) logger.error(`Unable to send Identity verification request status email to ${doc.username}`);
								});
							});
						}

						return this.toJSON(doc);
					});
			}
		}

	},

	methods: {},

	init(ctx) { },

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
		`,

		types: `
		`,

		mutation: `
		`,

		resolvers: {
		}
	}

};
