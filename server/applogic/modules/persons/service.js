"use strict";

let logger 		= require("../../../core/logger");
let config 		= require("../../../config");
let Sockets		= require("../../../core/sockets");
let C 	 		= require("../../../core/constants");

let _			= require("lodash");
let async 		= require("async");

let User 		= require("./models/user");
let blocktrail = require("blocktrail-sdk");
let Nexmo = require("nexmo");

const https = require("https");

module.exports = {
	settings: {
		name: "persons",
		version: 1,
		namespace: "persons",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: User
	},

	actions: {
		// return all model
		/*find: {
			cache: true,
			handler(ctx) {
				return ctx.queryPageSort(User.find({})).exec().then( (docs) => {
					return this.toJSON(docs);
				})
				.then((json) => {
					return this.populateModels(json);
				});
			}
		},*/

		// return a model by ID
		get: {
			cache: true,
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:UserNotFound"));
				return Promise.resolve(ctx.model);
			}
		},

		verifyPhone: {
			handler(ctx) {
				let nexmo = new Nexmo({
					apiKey: config.nexmo.apiKey,
					apiSecret: config.nexmo.apiSecret,
				});

				if (ctx.params.phoneNumber) {
					// test if is number
					let phoneNumber = ctx.params.phoneNumber;
					if (isNaN(parseInt(phoneNumber))) return;

					// if sms resend request
					if (ctx.params.resendSms === "1") {
						let path = `https://api.nexmo.com/verify/control/json?api_key=${config.nexmo.apiKey}&api_secret=${config.nexmo.apiSecret}&request_id=${ctx.user.phoneVerificationRequestId}&cmd=trigger_next_event`;
						https.get(path, res => {})
						.on("error", e => {
							logger.error(e);
						});
						return;
					}

					return new Promise((resolve, reject) => {
						nexmo.verify.request(
							{number: `234${phoneNumber}`, brand: config.app.title},
							(err, response) => {
								logger.debug(err);
								logger.debug(response);
								if (err) return logger.error(err);

								User.findByIdAndUpdate(
									ctx.user.id,
									{
										phone: phoneNumber,
										phoneVerificationRequestId: response.request_id
									},
									(err, user) => {
										if (err) return logger.error(err);
										else resolve({
											status: 200
										});
									}
								);
							}
						);
					});
				}

				if (ctx.params.phoneVerifyCode) {
					let code = ctx.params.phoneVerifyCode;
					if (code.length === 0) return "Please enter a valid verification code";

					return new Promise((resolve, reject) => {
						nexmo.verify.check(
							{request_id: ctx.user.phoneVerificationRequestId, code: code},
							(err, response) => {
								logger.debug(err);
								logger.debug(response);
								if (err) return logger.error(err);

								if (response.status !== "0") resolve(response);

								User.findByIdAndUpdate(
									ctx.user.id,
									{ phoneVerified: true },
									(err, user) => {
										if (err) return logger.error(err);
										else resolve({
											status: 200
										});
									}
								);
							}
						);

					});
				}
			}
		},

		newWalletAddress: {
			cache: true,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let client = blocktrail.BlocktrailSDK({
						apiKey: config.blocktrail.apiKey,
						apiSecret: config.blocktrail.apiSecret,
						network: "BTC",
						testnet: config.blocktrail.isTestnet
					});

					client.initWallet(
						ctx.user.walletName,
						ctx.user.walletPassword,
						(err, wallet) => {
							if (err) logger.error(err);
							else {
								wallet.getNewAddress((err, address) => {
									if (err) logger.error(err);
									else {
										User.findByIdAndUpdate(
											ctx.user.id,
											{ walletAddress: address },
											(err, user) => {
												if (err) logger.error(err);
												else resolve(address);
											}
										);
									}
								});
							}
						}
					);
				});
			}
		}
	},

	methods: {},

	graphql: {

		query: `
			# users(limit: Int, offset: Int, sort: String): [Person]
			person(code: String): Person
			newWalletAddress: String
		`,

		types: `
			type Person {
				code: String!
				email: String
				username: String
				walletAddress: String
				mainBalance: Int
				unconfirmedBalance: Int
				escrowedBalance: Int
				roles: [String]
				# avatar: String
				lastLogin: Timestamp
				# posts(limit: Int, offset: Int, sort: String): [Post]
			}
		`,

		mutation: `
		`,

		resolvers: {
			Query: {
				//users: "find",
				person: "get",
				newWalletAddress: "newWalletAddress"
			},

			// Person: {
			// 	posts(person, args, context) {
			// 		let ctx = context.ctx;
			// 		let postService = ctx.services("posts");
			// 		if (postService)
			// 			return postService.actions.find(ctx.copy(Object.assign(args, { author: person.code })));
			// 	}
			// }
		}
	}

};

/*
## GraphiQL test ##

# Get a person
query getPerson {
  person(code: "O5rNl5Bwnd") {
    ...personFields
  }
}


fragment personFields on Person {
  code
  fullName
  username
  roles
  avatar
  lastLogin

  posts(sort: "-createdAt") {
    code
    title
  }
}

*/
