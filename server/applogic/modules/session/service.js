"use strict";

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let _ = require("lodash");

let Sockets = require("../../../core/sockets");
let blocktrail = require("blocktrail-sdk");
let blocktrailClient = require("../../../core/blocktrailClient");

let Notification = require("../notifications/models/notification");
let User = require("../../../models/user");

let personService;

module.exports = {
	settings: {
		name: "session",
		version: 1,
		namespace: "session",
		rest: true,
		ws: true,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user"
	},

	actions: {

		// return my User model
		me(ctx) {
			return Promise.resolve(ctx.user).then((doc) => {
				return personService.toJSON(doc);
			});
		},

		// return all online users
		onlineUsers: {
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				return Promise.resolve().then(() => {
					return personService.toJSON(_.map(Sockets.userSockets, (s) => s.request.user));
				});
			}
		},

		// return all my transactions
		transactions: {
			cache: true,
			handler(ctx) {
				return new Promise((resolve, reject) => {
					let client = blocktrailClient.new();

					client.initWallet(
						ctx.user.walletName,
						ctx.user.walletPassword,
						(err, wallet) => {
							if (err) return logger.error(err);

							let filter = {
								sort_dir: "desc",
								limit: 10,
								page: ctx.params.page
							};
							wallet.transactions(filter, (err, result) => {
								if (err) return logger.error(err);
								resolve(result);
							});
						}
					);
				});
			}
		},

		notifications(ctx) {
			return new Promise((resolve, reject) => {
				let filter = {};
				filter.username = ctx.user.username;
				filter.read = false;

				if (ctx.params.action === "markAllAsRead") {
					Notification.update(filter,
						{ read: true }, { multi: true }, (err, count) => {
							resolve(count);
						});
				} else {
					let query = Notification.find(filter);

					ctx.params.limit = 5;
					ctx.params.sort = "-createdAt";

					ctx.queryPageSort(query).exec()
						.then(docs => {
							resolve(this.toJSON(docs));
						});
				}
			});
		},

		// send some coin
		sendCoin(ctx) {
			this.validateParams(ctx, true);

			return new Promise((resolve, reject) => {
				let client = blocktrailClient.new();

				client.initWallet(
					ctx.user.walletName,
					ctx.user.walletPassword,
					(err, wallet) => {
						if (err) return logger.error(err);

						let value = blocktrail.toSatoshi(ctx.params.btcAmount);
						let coinAddress = ctx.params.receiveAddress;
						let tx = new Object();
						tx[coinAddress] = value;
						wallet.pay(tx, null, true, true,
							blocktrail.Wallet.FEE_STRATEGY_LOW_PRIORITY,
							(err, result) => {
								if (err) return logger.error(err);
								resolve(result);
							}
						);
					}
				);
			});
		},

		settings(ctx) {
			let update = {};

			if (ctx.params.toggle === "newsletter")
				update.dailyPrice = !ctx.user.dailyPrice;

			if (ctx.params.toggle === "newTradeNotif")
				update.newTradeNotif = !ctx.user.newTradeNotif;

			if (ctx.params.toggle === "newMessageNotif")
				update.newMessageNotif = !ctx.user.newMessageNotif;

			if (ctx.params.toggle === "tradeStatusChangedNotif")
				update.tradeStatusChangedNotif = !ctx.user.tradeStatusChangedNotif;

			return User.findByIdAndUpdate(ctx.user.id, update, { new: true })
				.exec()
				.then((user) => {
					return personService.toJSON(user);
				});
		}
	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `sendCoin` action
		 * 
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("receiveAddress"))
				ctx.validateParam("receiveAddress").trim().notEmpty(ctx.t("app:TxAddressCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("btcAmount"))
				ctx.validateParam("btcAmount").notEmpty(ctx.t("app:TxAmountCannotBeEmpty")).end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		}
	},

	init(ctx) {
		personService = this.services("persons");
	},

	graphql: {

		query: `
			me: Person
			onlineUsers: [Person],
			# transactions: []
		`,

		types: `
			#type Transaction {
			#	
			#}
		`,

		mutation: `
		`,

		resolvers: {
			Query: {
				me: "me",
				onlineUsers: "onlineUsers"
			}
		}
	}

};

/*
## GraphiQL test ##

# Get my account
query me {
  me {
    ...personFields
  }
}


# Get list of online users
query getOnlineUser {
  onlineUsers {
    ...personFields
  }
}


fragment personFields on Person {
  code
  fullName
  email
  username
  roles
  verified
  avatar
  lastLogin
  locale
  
  posts(sort: "-createdAt") {
    code
    title
  }
}

*/