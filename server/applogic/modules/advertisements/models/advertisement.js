"use strict";

// let ROOT 			= "../../../../";
let config    		= require("../../../../config");
let logger    		= require("../../../../core/logger");

let _ 				= require("lodash");

let db	    		= require("../../../../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../../../../libs/hashids")("advertisements");
let autoIncrement 	= require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let AdvertisementSchema = new Schema({
	adType: {
		type: String,
		required: "Please specify the type of advertisement"
	},
	slug: {
		type: String,
		trim: true
	},
	title: {
		type: String,
		trim: true
	},
	price: {
		type: Number
	},
	minimumAmount: {
		type: Number
	},
	maximumAmount: {
		type: Number
	},
	originalMaximumAmount: {
		type: Number
	},
	bankName: {
		type: String,
		trim: true
	},
	bankAccountNumber: {
		type: String,
		trim: true
	},
	bankAccountName: {
		type: String,
		trim: true
	},
	receiveBitcoinAddress: {
		type: String,
		trim: true
	},
	username: {
		type: String,
		trim: true,
		ref: "User"
	},
	paymentWindow: {
		type: Number,
		default: 15
	},
	enabled: {
		type: Boolean,
		default: true
	},
	visible: {
		type: Boolean,
		default: false
	},
	metadata: {}

}, schemaOptions);

AdvertisementSchema.virtual("code").get(function() {
	return this.encodeID();
});

AdvertisementSchema.plugin(autoIncrement.plugin, {
	model: "Advertisement",
	startAt: 1
});

AdvertisementSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

AdvertisementSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

AdvertisementSchema.pre("save", function (next) {
	if (this.adType === "buy") {
		// users can buy as much as they want
		// hence, alignment should not affect buy ads
		this.maximumAmount = this.originalMaximumAmount;
		this.visible = true;
		return next();
	}

	let User = require("../../persons/models/user");
	User.findOne({ username: this.username }, (err, user) => {
		if (err) return next(err);

		if (user.computedBalance < this.minimumAmount)
			this.visible = false;
		else this.visible = true;

		let maxSellableAmount = (amount, price) => {
			return (amount * price) / (price + config.tradingFee);
		};

		this.maximumAmount = maxSellableAmount(this.originalMaximumAmount, this.price);

		if (user.computedBalance < this.maximumAmount)
			this.maximumAmount = maxSellableAmount(user.computedBalance, this.price);

		if (user.computedBalance > this.maximumAmount) {
			if (user.computedBalance > this.originalMaximumAmount)
				this.maximumAmount = maxSellableAmount(this.originalMaximumAmount, this.price);
			else
				this.maximumAmount = maxSellableAmount(user.computedBalance, this.price);
		}
		next();
	});
});

/*
PostSchema.static("getByID", function(id) {
	let query;
	if (_.isArray(id)) {
		query = this.collection.find({ _id: { $in: id} });
	} else
		query = this.collection.findById(id);

	return query
		.populate({
			path: "author",
			select: ""
		})
});*/

let Advertisement = mongoose.model("Advertisement", AdvertisementSchema);

module.exports = Advertisement;
