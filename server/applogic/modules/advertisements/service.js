"use strict";
// modelled after posts services

let logger = require("../../../core/logger");
let config = require("../../../config");
let C = require("../../../core/constants");

let async = require("async");
let _ = require("lodash");

let Advertisement = require("./models/advertisement");
let blocktrail = require("blocktrail-sdk");

module.exports = {
	settings: {
		name: "advertisements",
		version: 1,
		namespace: "advertisements",
		rest: true,
		ws: true,
		graphql: false,
		permission: C.PERM_LOGGEDIN,
		role: "user",
		collection: Advertisement,
	},

	actions: {
		find: {
			permission: C.PERM_PUBLIC,
			handler(ctx) {
				let filter = {};

				if (!ctx.params.slug) return;
				filter.slug = ctx.params.slug;

				return Advertisement.findOne(filter).exec()
					.then(doc => {
						return this.toJSON(doc);
					});
			}
		},

		create: {
			handler(ctx) {
				this.validateParams(ctx, true);

				let bankName = ctx.params.bankName;
				let title;
				if (ctx.params.adType === "buy") {
					title = `sell bitcoin to ${ctx.user.username} using ${bankName.replaceAll("_", " ")}`;

					// buyer must have completed at least 3 trades
					// before he can post a buy ad
					if (ctx.user.totalBuys < 3) return false;
				} else {
					title = `buy bitcoin at cheap rate using ${bankName.replaceAll("_", " ")} from ${ctx.user.username}`;
				}

				let advertisement = new Advertisement({
					adType: ctx.params.adType,
					title: title,
					slug: title.replaceAll(" ", "-") + "-" + this.randomStr(),
					price: ctx.params.price,
					minimumAmount: blocktrail.toSatoshi(ctx.params.minimumAmount),
					originalMaximumAmount: blocktrail.toSatoshi(ctx.params.maximumAmount),
					bankName: ctx.params.bankName,
					bankAccountNumber: ctx.params.bankAccountNumber,
					bankAccountName: ctx.params.bankAccountName,
					receiveBitcoinAddress: ctx.params.receiveBitcoinAddress,
					username: ctx.user.username,
					paymentWindow: ctx.params.paymentWindow
				});

				return advertisement.save()
					.then(doc => {
						return this.toJSON(doc);
					})

					// notify clients of new available ad
					// clients should refresh their list of ads accordingly
					.then(json => {
						this.notifyModelChanges(ctx, "created", json);
						return json;
					});
			}
		},

		// return all current logged in user's models
		me: {
			permission: C.PERM_OWNER,
			handler(ctx) {
				let filter = {};
				let query;
				filter.username = ctx.user.username;

				if (ctx.params.slug) {
					filter.slug = ctx.params.slug;
					query = Advertisement.findOne(filter);
				} else
					query = Advertisement.find(filter);

				return query.exec()
					.then(doc => {
						return this.toJSON(doc);
					});
			}
		},

		// return all advertisements
		all: {
			permission: C.PERM_PUBLIC,
			cache: true,
			handler(ctx) {
				let filter = {};

				filter.visible = true;
				filter.enabled = true;

				if (ctx.params.type)
					filter.adType = ctx.params.type;

				if (ctx.params.amount) {
					let amount = parseFloat(ctx.params.amount);
					filter.minimumAmount = { $lte: blocktrail.toSatoshi(amount) };
					filter.maximumAmount = { $gte: blocktrail.toSatoshi(amount) };
					// if logged in user, filter out current user's ads
					if (ctx.user)
						filter.username = { $ne: ctx.user.username };
				}

				if (ctx.params.username)
					filter.username = ctx.params.username;

				let query = Advertisement.find(filter);

				let asyncTasks = {};

				// get requested ads
				asyncTasks.data = callback => {
					ctx.queryPageSort(query).exec()
						.then(docs => {
							callback(null, docs);
						});
				};

				// get total ads count
				asyncTasks.total = callback => {
					Advertisement.count(filter, callback);
				};

				return new Promise((resolve, reject) => {
					async.series(asyncTasks, (err, results) => {
						if (err) return logger.error(err);
						resolve(results);
					});
				});
			}
		},

		update: {
			handler(ctx) {
				ctx.assertModelIsExist(ctx.t("app:AdvertisementNotFound"));
				this.validateParams(ctx);

				let bankName = ctx.params.bankName;
				let title;
				if (ctx.params.adType === "buy")
					title = `sell bitcoin to ${ctx.user.username} using ${bankName.replaceAll("_", " ")}`;
				else
					title = `buy bitcoin at cheap rate using ${bankName.replaceAll("_", " ")} from ${ctx.user.username}`;

				return this.collection.findById(ctx.modelID).exec()
					.then(doc => {
						let rand = doc.slug.split("-");
						rand = rand[rand.length - 1];

						doc.title = title;
						doc.slug = title.replaceAll(" ", "-") + "-" + rand;
						doc.price = ctx.params.price;
						doc.minimumAmount = blocktrail.toSatoshi(ctx.params.minimumAmount);
						doc.originalMaximumAmount = blocktrail.toSatoshi(ctx.params.originalMaximumAmount);
						doc.bankName = ctx.params.bankName;
						doc.bankAccountNumber = ctx.params.bankAccountNumber;
						doc.bankAccountName = ctx.params.bankAccountName;
						doc.receiveBitcoinAddress = ctx.params.receiveBitcoinAddress;
						doc.paymentWindow = ctx.params.paymentWindow;

						return doc.save();
					})
					.then(doc => {
						return this.toJSON(doc);
					});
			}
		},

		remove: {
			handler(ctx) {
				if (!ctx.params.code) return;

				let id = Advertisement.schema.methods.decodeID(ctx.params.code);
				return Advertisement.findByIdAndRemove(id, (err, advertisement) => {
					if (err) return logger.error(err);
					return advertisement;
				});
			}
		},

		disable: {
			handler(ctx) {
				if (!ctx.params.code) return;

				let id = Advertisement.schema.methods.decodeID(ctx.params.code);
				let update = {
					enabled: false
				};
				return Advertisement.findByIdAndUpdate(id, update).exec()
					.then( doc => {
						return this.toJSON(doc);
					});
			}
		},

		enable: {
			handler(ctx) {
				if (!ctx.params.code) return;

				let id = Advertisement.schema.methods.decodeID(ctx.params.code);
				let update = {
					enabled: true
				};
				return Advertisement.findByIdAndUpdate(id, update).exec()
					.then( doc => {
						return this.toJSON(doc);
					});
			}
		}

	},

	methods: {
		/**
		 * Validate params of context.
		 * We will call it in `create` and `update` actions
		 *
		 * @param {Context} ctx 			context of request
		 * @param {boolean} strictMode 		strictMode. If true, need to exists the required parameters
		 */
		validateParams(ctx, strictMode) {
			if (strictMode || ctx.hasParam("adType"))
				ctx.validateParam("adType").trim().notEmpty(ctx.t("app:AdvertisementTypeCannotBeEmpty")).end();

			if (strictMode || ctx.hasParam("price"))
				ctx.validateParam("price").isNumber();

			if (strictMode || ctx.hasParam("minimumAmount"))
				ctx.validateParam("minimumAmount").isNumber();

			if (strictMode || ctx.hasParam("maximumAmount"))
				ctx.validateParam("maximumAmount").isNumber();

			if (strictMode || ctx.hasParam("bankName"))
				ctx.validateParam("bankName").trim().notEmpty(ctx.t("app:AdvertisementBankNameCannotBeEmpty")).end();

			if (ctx.params.adType === "sell") {
				if (strictMode || ctx.hasParam("bankAccountNumber"))
					ctx.validateParam("bankAccountNumber").trim().notEmpty(ctx.t("app:AdvertisementBankAccountNumberCannotBeEmpty")).end();

				if (strictMode || ctx.hasParam("bankAccountName"))
					ctx.validateParam("bankAccountName").trim().notEmpty(ctx.t("app:AdvertisementBankAccountNameCannotBeEmpty")).end();
			} else {
				if (strictMode || ctx.hasParam("receiveBitcoinAddress"))
					ctx.validateParam("receiveBitcoinAddress").trim().notEmpty(ctx.t("app:AdvertisementReceiveBitcoinAddressCannotBeEmpty")).end();
			}

			if (strictMode || ctx.hasParam("paymentWindow"))
				ctx.validateParam("paymentWindow").notEmpty(ctx.t("app:AdvertisementPaymentWindowCannotBeEmpty")).parseInt().end();

			if (ctx.hasValidationErrors())
				throw ctx.errorBadRequest(C.ERR_VALIDATION_ERROR, ctx.validationErrors);
		},

		/**
		 * Generate a random 5 digits string
		 *
		 */
		randomStr() {
			let text = "";
			let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

			for (let i = 0; i < 5; i++)
				text += possible.charAt(Math.floor(Math.random() * possible.length));

			return text;
		}

	},

	/**
	 * Check the owner of model
	 * 
	 * @param {any} ctx	Context of request
	 * @returns	{Promise}
	 */
	// ownerChecker(ctx) {
	// 	return new Promise((resolve, reject) => {
	// 		ctx.assertModelIsExist(ctx.t("app:AdvertisementNotFound"));

	// 		if (ctx.model.username == ctx.user.username || ctx.isAdmin()) 
	// 			resolve();
	// 		else
	// 			reject();
	// 	});
	// },

	init(ctx) {
		// register new sting function used in `create` action
		String.prototype.replaceAll = function (search, replacement) {
			let target = this;
			return target.replace(new RegExp(search, "g"), replacement);
		};
	},

	socket: {
		afterConnection(socket, io) {
			// Fired when a new client connected via websocket
		}
	},

	graphql: {

		query: `
		`,

		types: `
		`,

		mutation: `
		`,

		resolvers: {
		}
	}

};

/*
## GraphiQL test ##

# Find all posts
query getPosts {
  posts(sort: "-createdAt -votes", limit: 3) {
    ...postFields
  }
}

# Create a new post
mutation createPost {
  postCreate(title: "New post", content: "New post content") {
    ...postFields
  }
}

# Get a post
query getPost($code: String!) {
  post(code: $code) {
    ...postFields
  }
}

# Update an existing post
mutation updatePost($code: String!) {
  postUpdate(code: $code, content: "Modified post content") {
    ...postFields
  }
}

# vote the post
mutation votePost($code: String!) {
  postVote(code: $code) {
    ...postFields
  }
}

# unvote the post
mutation unVotePost($code: String!) {
  postUnvote(code: $code) {
    ...postFields
  }
}

# Remove a post
mutation removePost($code: String!) {
  postRemove(code: $code) {
    ...postFields
  }
}



fragment postFields on Post {
    code
    title
    content
    author {
      code
      fullName
      username
      avatar
    }
    views
    votes
  	voters {
  	  code
  	  fullName
  	  username
  	  avatar
  	}
}

*/
