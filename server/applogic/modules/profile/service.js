"use strict";

let logger 		= require("../../../core/logger");
let config 		= require("../../../config");
let C 	 		= require("../../../core/constants");

let _			= require("lodash");

let User 		= require("./models/user");

module.exports = {
	settings: {
		name: "profile",
		version: 1,
		namespace: "profile",
		rest: true,
		ws: true,
		graphql: true,
		permission: C.PERM_PUBLIC,
		role: "user",
		collection: User
	},

	actions: {
		get: {
			cache: true,
			handler(ctx) {
				if (!ctx.params.username) return false;
				let filter = {};
				filter.username = ctx.params.username;

				return User.findOne(filter).exec().then( (doc) => {
					return this.toJSON(doc);
				});
				// .then((json) => {
				// 	return this.populateModels(json);
				// });
			}
		}
	},

	methods: {
	},

	graphql: {

		query: `
			profile: Profile
		`,

		types: `
			type Profile {
				code: String!
				email: String
				username: String
				passwordLess: Boolean
				provider: String
				profile: SocialProfile
				socialLinks: SocialLinks
				roles: [String]
				verified: Boolean
				apiKey: String
				locale: String
				avatar: String
				createdAt: Timestamp
				updatedAt: Timestamp
				lastLogin: Timestamp
				status: Boolean
			}

			type SocialProfile {
				name: String
				gender: String
				picture: String
				location: String
			}

			type SocialLinks {
				facebook: String
				twitter: String
				google: String
				github: String
			}
		`,

		mutation: `
		`,

		resolvers: {
			Query: {
				profile: "get"
			}
		}
	}

};

/*
## GraphiQL test ##

# Get a person
query getPerson {
  person(code: "O5rNl5Bwnd") {
    ...personFields
  }
}


fragment personFields on Person {
  code
  fullName
  email
  username
  roles
  verified
  avatar
  lastLogin
  locale

  posts(sort: "-createdAt") {
    code
    title
  }
}

*/
