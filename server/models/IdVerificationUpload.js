"use strict";

let config    		= require("../config");
let logger    		= require("../core/logger");
let C 				= require("../core/constants");
let fs 				= require("fs");
let path 			= require("path");

let db	    		= require("../core/mongo");
let mongoose 		= require("mongoose");
let Schema 			= mongoose.Schema;
let hashids 		= require("../libs/hashids")("users");
let autoIncrement 	= require("mongoose-auto-increment");

let schemaOptions = {
	timestamps: true,
	toObject: {
		virtuals: true
	},
	toJSON: {
		virtuals: true
	}
};

let IdVerificationUploadSchema = new Schema({
	username: {
		type: String,
		trim: true,
		ref: "User"
	},
	
	imageUrl: {
		type: String,
		required: true
	},

	metadata: {},

}, schemaOptions);

/**
 * Virtual `code` field instead of _id
 */
IdVerificationUploadSchema.virtual("code").get(function() {
	return this.encodeID();
});

/**
 * Auto increment for `_id`
 */
IdVerificationUploadSchema.plugin(autoIncrement.plugin, {
	model: "IdVerificationUpload",
	startAt: 1
});

/**
 * Encode `_id` to `code`
 */
IdVerificationUploadSchema.methods.encodeID = function() {
	return hashids.encodeHex(this._id);
};

/**
 * Decode `code` to `_id`
 */
IdVerificationUploadSchema.methods.decodeID = function(code) {
	return hashids.decodeHex(code);
};

let IdVerificationUpload = mongoose.model("IdVerificationUpload", IdVerificationUploadSchema);

module.exports = IdVerificationUpload;
