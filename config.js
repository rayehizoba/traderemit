"use strict";

let path = require("path");
let pkg = require("./package.json");

module.exports = {

	// Secret for ID hashing
	hashSecret: "0ONo2vYdhPG6KD77BG1tAwiPzfMQ7uNjfNftPERtheb",

	// Secret for session hashing
	sessionSecret: "GTkRzPWbN10iXkiXTuhIraYyUrZNk6MhCqqi2AarHuS",

	// Application settings
	app: {
		//title: "VEM APP",
		//version: "1.0.0",
		//description: "This is my boilerplate web app",
		//keywords: "boilerplate, starter, webapp",
		//url: "http://localhost:3000/",
		// googleAnalyticsID: "UA-xxxxxxxxx-x",
		//contactEmail: "hello@vem-app.com"
	},

	// ip: process.env.NODE_IP || "0.0.0.0",
	// port: process.env.NODE_PORT || 3000,

	// dataFolder: path.join(global.rootPath, "data"),
	// logFolder: path.join(global.rootPath, "logs"),

	// Database (Mongo) settings
	db: {
		// uri: process.env.MONGO_URI || "mongodb://localhost/vemapp",
		options: {
			user: process.env.MONGO_USERNAME || "",
			pass: process.env.MONGO_PASSWORD || ""
		}
	},

	// Redis settings for caching
	// redis: {
	// 	enabled: false,
	// 	uri: process.env.REDIS_URI || "redis://localhost:6379",
	// 	options: null
	// },

	// Mail sending settings
	// mailer: {
	// 	enabled: true, // change this flag to true to turn emailing feature on.

	// 	//if enabled = true make sure to configure one of the methods below
	// 	from: "TradeRemit <noreply@traderemit.com>",

	// 	/*transport: "smtp",
	// 	smtp: {
	// 		host: "smtp.mailtrap.io",
	// 		port: 2525,
	// 		auth: {
	// 			user: "dca08f157a6938",
	// 			pass: "b5c02413581409"
	// 		}
	// 	}*/

	// 	/*transport: "smtp",
	// 	smtp: {
	// 		host: "smtp.gmail.com",
	// 		port: 465,
	// 		secure: true,
	// 		auth: {
	// 			user: "",
	// 			pass: ""
	// 		}
	// 	}*/

	// 	transport: "mailgun",
	// 	mailgun: {
	// 		apiKey: 'key-da7fbbcd296887021da3bf049ce0927e',
	// 		domain: 'sandbox8b0f749b2ba94898a842338eda935a58.mailgun.org'
	// 	}

	// 	/*
	// 	transport: "sendgrid",
	// 	sendgrid: {
	// 		apiKey: ""
	// 	}*/
	// },

	// Features of application
	features: {
		disableSignUp: false,
		verificationRequired: true
	},

	// Social authentication (OAuth) keys
	authKeys: {

		google: {
			clientID: null,
			clientSecret: null
		},

		facebook: {
			clientID: null,
			clientSecret: null
		},

		github: {
			clientID: null,
			clientSecret: null
		},

		twitter: {
			clientID: null,
			clientSecret: null
		}
	},

	// Logging settings
	logging: {

		console: {
			// level: "debug"
		},

		file: {
			enabled: false,
			// path: path.join(global.rootPath, "logs"),
			// level: "info",
			// json: false,
			// exceptionsSeparateFile: true
		},

		graylog: {
			enabled: false
			// servers: [ { host: "192.168.0.100", port: 12201 } ]
		},

		papertrail: {
			enabled: false,
			host: null,
			port: null,
			level: "debug",
			program: "vem"
		},

		logentries: {
			enabled: false,
			token: null
		},

		loggly: {
			enabled: false,
			token: null,
			subdomain: null
		},

		logsene: {
			enabled: false,
			token: null
		},

		logzio: {
			enabled: false,
			token: null
		}

	},

	blocktrail: {
		apiKey: "9355fe6f5ffe32deea596a2f47a0e9646dedec15",
		apiSecret: "431c7eb763dd4ef2acafc71bcbcb00d5dfba6d10",
		webhookLabel: "traderemit-webhooks",
		webhookToken: "ySjyLuiFgexheOsRcpbP7INSbc2PDvXN"
	},

	tradingFee: 2, // never forget to also update this on the client

	nexmo: {
		apiKey: "d64930b9",
		apiSecret: "ef3ee4253394eafc",
		applicationId: "27e79fea-9475-474f-8cb6-51224aec2dd9",
		answerUrl: "https://gitlab.com/rayehizoba/traderemit/raw/master/traderemit_NCCO.json?private_token=5s7MHx7FJHHzJDXrDJyX"
	},
	
	admin: {
		email: "admin@traderemit.com",
		password: "l0v3d0c7o2{[]}"
	},

	cloudinary: {
		cloudName: 'traderemit',
		apiKey: '392525334458229',
		apiSecret: 'GXNgrQLE6LaLK6v5SvoDoi2eQYc'
	},

};
