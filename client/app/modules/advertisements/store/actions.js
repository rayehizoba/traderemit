import Vue from "vue";
import axios from "axios";
import Service from "../../../core/service";
import toastr from "../../../core/toastr";
import { LOAD, ADD, UPDATE, REMOVE } from "./types";
import router from "../../../core/router";

export const NAMESPACE = "/api/advertisements";

let service = new Service("advertisements");

export const saveAdvertisement = ({ commit }, form) => {
	form.post(`${NAMESPACE}/create`).then((response) => {
		let res = response.data;
		if (res.status === 200) {
			// update my ads store
			created({ commit }, res.data);
			router.push({ path: `/dashboard/your-advertisements/${res.data.slug}` });
		}
	}).catch((response) => {
		if (response.data.error)
			toastr.error(response.data.error.message);
	});
};

export const updateAdvertisement = ({ commit }, advertisement) => {
	axios.post(`${NAMESPACE}/update`, advertisement).then((response) => {
		let res = response.data;
		if (res.status === 200) {
			// update my ads store
			updated({ commit }, res.data);
			router.push({ path: `/dashboard/your-advertisements/${res.data.slug}` });
		}
	}).catch((response) => {
		if (response.data.error)
			toastr.error(response.data.error.message);
	});
};

export const created = ({ commit }, row) => {
	commit(ADD, row);
};

export const updated = ({ commit }, row) => {
	commit(UPDATE, row);
};

export const downloadAll = ({ commit }) => {
	service.rest("me")
		.then(data => {
			commit(LOAD, data);
		}).catch(err => {
			console.log("Response err ", err);
		});
};
