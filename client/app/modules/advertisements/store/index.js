import { LOAD, ADD, UPDATE, REMOVE } from "./types";
import { find, findIndex, each } from "lodash";

const state = {
	buy: [],
	sell: []
};

const mutations = {
	[LOAD](state, models) {
		state.buy.splice(0);
		state.sell.splice(0);
		each(models, item => {
			if (item.adType === "buy")
				state.buy.push(item);
			else
				state.sell.push(item);
		});
	},

	[ADD](state, model) {
		if (model.adType === "buy") {
			let found = find(state.buy, (item) => item.code == model.code);
			if (!found)
				state.buy.push(model);
		} else {
			let found = find(state.sell, (item) => item.code == model.code);
			if (!found)
				state.sell.push(model);
		}
	},

	[UPDATE](state, model) {
		if (model.adType === "buy") {
			let index = findIndex(state.buy, { code: model.code });
			state.buy.splice(index, 1, model);
		} else {
			let index = findIndex(state.sell, { code: model.code });
			state.sell.splice(index, 1, model);
		}
	}

};

import * as getters from "./getters";
import * as actions from "./actions";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
