import {
	UPDATE_USD_RATE,
	TOGGLE_PRICE_DISPLAY
} from "./types";

const state = {
	usdRate: 0,
	priceDisplayNGNBTC: false
};

const mutations = {
	[UPDATE_USD_RATE] (state, value) {
		state.usdRate = value;
	},

	[TOGGLE_PRICE_DISPLAY] (state) {
		state.priceDisplayNGNBTC = !state.priceDisplayNGNBTC;
	},
};

import * as getters from "./getters";
import * as actions from "./actions";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};