import Vue from "vue";
import axios from "axios";
import {
	UPDATE_USD_RATE,
	TOGGLE_PRICE_DISPLAY
} from "./types";

export const getUsdRate = ({ commit }) => {
	axios.get("https://bitpay.com/api/rates/usd").then((response) => {
		if (response && response.status === 200) {
			commit(UPDATE_USD_RATE, response.data.rate);
		}
	}).catch((response) => {
		alert("Could not get latest market bitUSD rate. Please check your internet connection and reload this page.");
		location.reload();
	});
};

export const togglePriceDisplayNGNBTC = ({ commit }) => {
	commit(TOGGLE_PRICE_DISPLAY);
};
