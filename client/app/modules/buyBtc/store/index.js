import { LOAD, PAGINATING } from "./types";

const state = {
	all: [],
	cheapest: {},
	meta: {
		loading: true,
		offset: 0,
		limit: 5,
		total: 0,
		paginating: true
	}
};

const mutations = {
	[LOAD](state, payload) {
		let data = payload.data;
		let query = payload.query;
		state.meta.loading = false;
		state.meta.paginating = false;
		state.all.splice(0);
		state.all.push(...data.data);
		state.meta.total = data.total;
		if (query.offset === 0) {
			state.cheapest = state.all[0];
		}
	},

	[PAGINATING](state, flag) {
		state.meta.paginating = flag;
	}
};

import * as getters from "./getters";
import * as actions from "./actions";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};