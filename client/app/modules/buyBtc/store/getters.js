export function buyAds(state) {
	return state.all;
}

export function buyAdsMeta(state) {
	return state.meta;
}

export function cheapestBuyAd(state) {
	return state.cheapest;
}