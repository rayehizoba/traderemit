export function me(state) {
	return state.user;
}

export function onlineUsers(state) {
	return state.onlineUsers;
}

export function unreadNotifications(state) {
	return state.notifications;
}

export function transactions(state) {
	return state.transactions;
}

export function searchText(state) {
	return state.searchText;
}
