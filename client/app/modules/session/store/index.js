import {
	ADD_NOTIFICATION,
	SET_NOTIFICATIONS,
	SET_USER,
	SEARCH,
	UPDATE_WALLET_BALANCE,
	SET_WALLET_TRANSACTIONS,
	MARK_ALL_NOTIFICATIONS_AS_READ,
	SET_ONLINE_USERS
} from "./types";

const state = {
	user: null,
	notifications: [],
	searchText: "",
	transactions: {
		loading: true,
		paginating: false,
		data: [],
		page_meta: {
			current_page: 1,
			total: null
		},
	},
	onlineUsers: null
};

const mutations = {

	[ADD_NOTIFICATION] (state, item) {
		state.notifications.unshift(item);
	},

	[SET_NOTIFICATIONS] (state, notifications) {
		state.notifications = notifications;
	},

	[MARK_ALL_NOTIFICATIONS_AS_READ] (state) {
		state.notifications = [];
	},

	[SET_USER] (state, user) {
		state.user = user;
	},

	[SET_ONLINE_USERS] (state, onlineUsers) {
		state.onlineUsers = onlineUsers;
	},

	[SEARCH] (state, text) {
		state.searchText = text;
	},

	[UPDATE_WALLET_BALANCE] (state, item) {
		state.user.mainBalance = item.mainBalance;
		state.user.computedBalance = item.computedBalance;
		state.user.escrowedBalance = item.escrowedBalance;
		state.user.unconfirmedBalance = item.unconfirmedBalance;
	},

	[SET_WALLET_TRANSACTIONS] (state, data) {
		state.transactions.data = data.data;
		state.transactions.page_meta.current_page = data.current_page;
		state.transactions.page_meta.total = Math.ceil(data.total / data.per_page);
	}

};

import * as getters from "./getters";
import * as actions from "./actions";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};
