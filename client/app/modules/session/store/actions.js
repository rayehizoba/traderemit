import {
	ADD_NOTIFICATION,
	SET_NOTIFICATIONS,
	SET_USER,
	SET_ONLINE_USERS,
	SEARCH,
	UPDATE_WALLET_BALANCE,
	SET_WALLET_TRANSACTIONS,
	MARK_ALL_NOTIFICATIONS_AS_READ,
} from "./types";
import Service from "../../../core/service";

let service = new Service("session");

export const getSessionUser = ({ commit }) => {
	service.rest("me")
		.then(data => {
			commit(SET_USER, data);
		}).catch(err => {
			console.log("Response err ", err);
		});
};

export const getOnlineUsers = ({ commit }) => {
	service.rest("onlineUsers")
		.then(data => {
			commit(SET_ONLINE_USERS, data);
		}).catch(err => {
			console.log("Response err ", err);
		});
};

export const setOnlineUsers = ({ commit }, onlineUsers) => {
	commit(SET_ONLINE_USERS, onlineUsers);
};

export const getNotifications = ({ commit }) => {
	// get last 5 unread notifications
	service.rest("notifications")
		.then(data => {
			commit(SET_NOTIFICATIONS, data);
		}).catch(err => {
			console.log("Response err ", err);
		});
};

export const setAllNotificationAsRead = ({ commit }) => {
	service.rest("notifications?action=markAllAsRead");
	commit(MARK_ALL_NOTIFICATIONS_AS_READ);
};

export const addNotification = ({ commit }, item) => {
	commit(ADD_NOTIFICATION, item);
};

export const searching = ({ commit }, text) => {
	commit(SEARCH, text);
};

export const updateWalletBalance = ({ commit }, item) => {
	commit(UPDATE_WALLET_BALANCE, item);
};

export const getTransactions = (ctx, page = 1) => {
	service.rest(`transactions?page=${page}`)
		.then(data => {
			ctx.state.transactions.loading = false;
			ctx.state.transactions.paginating = false;
			ctx.commit(SET_WALLET_TRANSACTIONS, data);
		}).catch(err => {
			ctx.state.transactions.loading = false;
			ctx.state.transactions.paginating = false;
			console.log("Response err ", err);
		});
};

export const toggleNewsletter = ({ commit }) => {
	service.rest("settings?toggle=newsletter")
		.then(data => {
			commit(SET_USER, data);
		}).catch(err => {
			console.log("Response err ", err);
		});
};

export const toggleNewTradeNotif = ({ commit }) => {
	service.rest("settings?toggle=newTradeNotif")
		.then(data => {
			commit(SET_USER, data);
		}).catch(err => {
			console.log("Response err ", err);
		});
};

export const toggleNewMessageNotif = ({ commit }) => {
	service.rest("settings?toggle=newMessageNotif")
		.then(data => {
			commit(SET_USER, data);
		}).catch(err => {
			console.log("Response err ", err);
		});
};

export const toggleTradeStatusChangedNotif = ({ commit }) => {
	service.rest("settings?toggle=tradeStatusChangedNotif")
		.then(data => {
			commit(SET_USER, data);
		}).catch(err => {
			console.log("Response err ", err);
		});
};
