import Vue from "vue";
import { LOAD, PAGINATING } from "./types";
import axios from "axios";

export const NAMESPACE = "/api/advertisements/all?type=sell";

function fetch(query) {
	return axios.get(`${NAMESPACE}&sort=price&limit=${query.limit}&offset=${query.offset}`);
}

export const downloadRows = ({ commit }, query) => {
	fetch(query).then((response) => {
		let res = response.data;
		if (res.status == 200 && res.data)
			commit(LOAD, { data: res.data, query: query });
		else
			console.error("Request error!", res.error);

	}).catch((response) => {
		console.error("Request error!", response.statusText);
	});

};

export const downloadNextPage = ({ commit }, query) => {
	commit(PAGINATING, true);
	query.offset += query.limit;
	fetch(query).then((response) => {
		let res = response.data;
		if (res.status == 200 && res.data)
			commit(LOAD, { data: res.data, query: query });
		else
			console.error("Request error!", res.error);

	}).catch((response) => {
		console.error("Request error!", response.statusText);
	});
};

export const downloadPrevPage = ({ commit }, query) => {
	commit(PAGINATING, true);
	query.offset -= query.limit;
	fetch(query).then((response) => {
		let res = response.data;
		if (res.status == 200 && res.data)
			commit(LOAD, { data: res.data, query: query });
		else
			console.error("Request error!", res.error);

	}).catch((response) => {
		console.error("Request error!", response.statusText);
	});
};
