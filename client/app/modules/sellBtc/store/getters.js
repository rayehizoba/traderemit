export function sellAds(state) {
	return state.all;
}

export function sellAdsMeta(state) {
	return state.meta;
}

export function cheapestSellAd(state) {
	return state.cheapest;
}