import Vue from "vue";
import {
	LOAD_TRADES,
	UPDATE_OPEN_TRADES
} from "./types";
import axios from "axios";
import router from "../../../core/router";

export const NAMESPACE = "/api/trades";

export const downloadTrades = ({ commit }) => {
	axios.get(`${NAMESPACE}/all`).then((response) => {
		let res = response.data;
		if (res.status == 200 && res.data)
			commit(LOAD_TRADES, res.data);
		else
			console.error("Request error!", res.error);

	}).catch((response) => {
		console.error("Request error!", response.statusText);
	});

};

export const createTrade = ({ commit }, form) => {

	form.post(`${NAMESPACE}/create`).then((response) => {
		let res = response.data;
		if (res.status === 200) {
			// update my open trades
			commit(UPDATE_OPEN_TRADES, res.data);
			// push me to this trade page
			router.push({ path: `/trade/${res.data.code}` });
		} else
			console.error("Request error!", res.error);
	}).catch((response) => {
		console.error("Request error!", response.statusText);
	});

};
