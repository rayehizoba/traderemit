import {
	LOAD_TRADES,
	LOAD_CLOSED_TRADES,
	UPDATE_OPEN_TRADES
} from "./types";

const state = {
	open: [],
	closed: [],
};

const mutations = {
	[LOAD_TRADES](state, trades) {
		state.open.splice(0);
		state.open.push(...trades.open);
		state.closed.splice(0);
		state.closed.push(...trades.closed);
	},

	[UPDATE_OPEN_TRADES] (state, data) {
		state.open.unshift(data);
	}
};

import * as getters from "./getters";
import * as actions from "./actions";

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};