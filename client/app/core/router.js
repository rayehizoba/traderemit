"use strict";

import Vue from "vue";
import VueRouter from "vue-router";

import Home from "../modules/home";
import Profile from "../modules/profile";

import Wallet from "../modules/wallet/index";

import Advert from "../modules/advertisements/index";
import PostAdvert from "../modules/advertisements/post";
import EditAdvert from "../modules/advertisements/edit";
import ViewAdvert from "../modules/advertisements/view";

import Dashboard from "../modules/dashboard/index";
import MyAdvertisement from "../modules/dashboard/viewAd";

import BuyBtc from "../modules/buyBtc/index";
import SellBtc from "../modules/sellBtc/index";

import ViewTrade from "../modules/trades/index";

import Notifications from "../modules/notifications/index";

import Settings from "../modules/settings/index";

import About from "../modules/about/index";

import Legalandpolicy from "../modules/legal_and_policy/index";

import IdentityVerification from "../modules/identity_verification/index";

Vue.use(VueRouter);

export default new VueRouter({
	mode: "history",
	routes: [
		{ path: "/", component: Home },
		{
			path: "/profile/:username",
			props: true,
			component: Profile
		},
		{
			path: "/wallet",
			name: "Wallet",
			component: Wallet
		},
		{
			path: "/wallet/*",
			name: "Wallet",
			component: Wallet
		},
		{
			path: "/advertisements",
			component: Advert,
			children: [
				{
					path: "create",
					component: PostAdvert
				},
				{
					path: "edit/:slug",
					props: true,
					component: EditAdvert
				},
				{
					path: ":slug",
					props: true,
					name: "View Ad",
					component: ViewAdvert,
				}
			]
		},
		{
			path: "/dashboard",
			name: "Dashboard",
			component: Dashboard
		},
		{
			path: "/dashboard/your-advertisements/:slug",
			props: true,
			name: "View My Advertisement",
			component: MyAdvertisement
		},
		{
			path: "/dashboard/*",
			name: "Dashboard",
			component: Dashboard
		},
		{
			path: "/trade/buy-bitcoin",
			name: "Buy Bitcoins",
			component: BuyBtc
		},
		{
			path: "/trade/sell-bitcoin",
			name: "Sell Bitcoins",
			component: SellBtc
		},
		{
			path: "/trade/:slug",
			props: true,
			component: ViewTrade
		},
		{
			path: "/notifications",
			component: Notifications
		},
		{
			path: "/settings",
			component: Settings
		},
		{
			path: "/about",
			component: About
		},
		{
			path: "/legal-and-policy",
			component: Legalandpolicy
		},
		{
			path: "/identity-verification",
			component: IdentityVerification
		},
		// { path: "/users", component: User, meta: { needRole: "admin" } },
		//{ path: "*", component: NotFound }
	],
	scrollBehavior(to, from, savedPosition) {
		let hasName = to.name && from.name;
		let isSameSubCategory = from.name === to.name;
		if (hasName && isSameSubCategory) return;
		if (savedPosition) {
			return savedPosition;
		} else {
			return { x: 0, y: 0 };
		}
	}
});
