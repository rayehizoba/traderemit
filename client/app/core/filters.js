import moment from "moment";
//import prettyBytes from "pretty-bytes";


let filters = {
	// prettyJSON(json) {
	// 	if (json) {
	// 		json = JSON.stringify(json, undefined, 4);
	// 		json = json.replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;");
	// 		return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
	// 			let cls = "number";
	// 			if (/^"/.test(match)) {
	// 				if (/:$/.test(match)) {
	// 					cls = "key";
	// 				} else {
	// 					cls = "string";
	// 				}
	// 			} else if (/true|false/.test(match)) {
	// 				cls = "boolean";
	// 			} else if (/null/.test(match)) {
	// 				cls = "null";
	// 			}
	// 			return "<span class=\"" + cls + "\">" + match + "</span>";
	// 		});
	// 	}
	// },

	/*bytes(bytes) {
		return prettyBytes(bytes);
	},*/

	ago(time) {
		return moment(time).fromNow();
	},

	truncate(text, length) {
		if (text && text.length > length)
			return text.substr(0, length - 1) + "…";
		return text;
	},

	satoshi2btc(value) {
		let digits = parseInt(value) / 100000000;

		let multiplier = Math.pow(10, 4), // 4 is to chop of digits after the 4th trailing float
			adjustedNum = digits * multiplier,
			truncatedNum = Math[adjustedNum < 0 ? "ceil" : "floor"](adjustedNum);

		return truncatedNum / multiplier;
		// return Number(digits.toPrecision(4));
	},

	snake2Lower(value) {
		return value.replace(new RegExp("_", "g"), " ");
	},

	uppercase(value) {
		let response = new String(value);
		return response.toUpperCase();
	},

	lowercase(value) {
		let response = new String(value);
		return response.toLowerCase();
	},

	currency(value, currency, decimals) {
		let digitsRE = /(\d{3})(?=\d)/g;
		value = parseFloat(value);
		if (!isFinite(value) || (!value && value !== 0)) return "";
		currency = currency != null ? currency : "$";
		decimals = decimals != null ? decimals : 2;
		let stringified = Math.abs(value).toFixed(decimals);
		let _int = decimals
			? stringified.slice(0, -1 - decimals)
			: stringified;
		let i = _int.length % 3;
		let head = i > 0
			? (_int.slice(0, i) + (_int.length > 3 ? "," : ""))
			: "";
		let _float = decimals
			? stringified.slice(-1 - decimals)
			: "";
		let sign = value < 0 ? "-" : "";
		return sign + currency + head +
			_int.slice(i).replace(digitsRE, "$1,") +
			_float;
	}

};

export default {
	filters,

	install(Vue) {
		let keys = Object.keys(filters);
		keys.forEach(name => Vue.filter(name, filters[name]));
	}
};
