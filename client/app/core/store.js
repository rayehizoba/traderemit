import Vue from "vue";
import Vuex from "vuex";

import session from "../modules/session/store";
import profile from "../modules/profile/store";
import trades from "../modules/trades/store";
import advertisements from "../modules/advertisements/store";
import sellAdvertisements from "../modules/sellBtc/store";
import buyAdvertisements from "../modules/buyBtc/store";
import globalState from "../modules/global/store";

Vue.use(Vuex);

export default new Vuex.Store({
	modules: {
		session,
		profile,
		trades,
		advertisements,
		sellAdvertisements,
		buyAdvertisements,
		globalState
	}
});