Before building and publishing to production, always remember to update the
version from the package.json file. This helps with cache busting (i.e. getting
the latest build on all clients).